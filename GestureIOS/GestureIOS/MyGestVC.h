//
//  MyGestVC.h
//  GestureIOS
//

//
@class GADBannerView;
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SPLockScreen.h"
#import "RecordingGestureVC.h"
#import "ProgressHUD.h"
#import "LoginVC.h"
@interface MyGestVC : UIViewController{
    AppDelegate *appdelegate;
    SPLockScreen *splock;
}
typedef enum {
    InfoStatusFirstTimeSetting = 0,
    InfoStatusConfirmSetting,
    InfoStatusFailedConfirm,
    InfoStatusNormal,
    InfoStatusFailedMatch,
    InfoStatusSuccessMatch
}	InfoStatus;

@property (weak, nonatomic) IBOutlet UIImageView *restartSymbol;
@property (weak, nonatomic) IBOutlet UIImageView *continueSymbol;

@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet SPLockScreen *lockScreenView;
@property (nonatomic) InfoStatus infoLabelStatus;

@property (weak, nonatomic) IBOutlet UIButton *restartButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIView *gestureView;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@property (nonatomic) NSString *pattern;
@property (nonatomic) NSString *recordType;
@property (nonatomic) NSString *recordObjId;
@property (nonatomic) NSData *uploadfile;
@property (nonatomic) NSString *isAddfromList;
@property (nonatomic) NSURL *videoURl;

- (IBAction)restsrtButton_click:(id)sender;
- (IBAction)continuebutton_click:(id)sender;

@end
