//
//  TermsVC.m
//  GestureIOS
//
//

#import "TermsVC.h"

@interface TermsVC ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation TermsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Terms & Conditions";
    
    NSURL *termsURL = [[NSBundle mainBundle] URLForResource:@"GestURe - Terms" withExtension:@"rtf"];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:termsURL]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _webView.frame = self.view.bounds;
}

@end