//
//  RecordingListTV.m
//  GestureIOS
//

//

#import "RecordingListTV.h"
#import "RecordCell.h"
#import "MyGestVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import "PlayGestureVC.h"
#import "NSObject+Async.h"
#import "FAQVC.h"

@interface RecordingListTV ()<AVAudioRecorderDelegate, AVAudioPlayerDelegate>{
    RecordCell *recordCell;
    NSArray *recordList;
    NSString * noOffav;
    NSString *noofsee;
    AVAudioPlayer *player;
    MPMoviePlayerViewController *moviePlayer;
    NSArray *paths;
    NSString *documentsDirectory ;
    NSString *path;
    NSURL *moveUrl;
    PFUser *currentUser;
    BOOL alreadyLoaded;
}

@end

@implementation RecordingListTV

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"My Status";

    UIBarButtonItem *exitButton = [[UIBarButtonItem alloc] initWithTitle:@"GestURe" style:UIBarButtonItemStyleBordered target:self action:@selector(Exitstatus:)];
    [self.navigationItem setLeftBarButtonItem:exitButton];
    
    UIBarButtonItem *faqButton = [[UIBarButtonItem alloc] initWithTitle:@"FAQ" style:UIBarButtonItemStyleBordered target:self action:@selector(triggerFAQ)];
    [self.navigationItem setRightBarButtonItem:faqButton];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(loadGesture)
                  forControlEvents:UIControlEventValueChanged];
    [self.refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationController.navigationBar.translucent = NO;
    
}

- (void) triggerFAQ {
    FAQVC *vc = [[FAQVC alloc] initWithNibName:@"FAQVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) Exitstatus : (id) sender{
//    MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
//    gest.isAddfromList = @"yes";
//    [self.navigationController pushViewController:gest animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) addNewgesture{
    MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
    gest.isAddfromList = @"yes";
    [self.navigationController pushViewController:gest animated:YES];
}

- (void) loadGesture {
    [self loadGestureWithCompletion:nil];
}

-(void) loadGestureWithCompletion:(void(^)(void))completion{
    PFQuery *query = [PFQuery queryWithClassName:@"RecordingFiles"];
     [query whereKey:@"userObjId" equalTo:currentUser.objectId];
    [query orderByDescending:@"createdAt"];

    [ProgressHUD show:@"Loading..."];
    self.navigationController.view.userInteractionEnabled = NO;
    
    [query performSelectorAsync:@selector(findObjects) completion:^(id result) {
        
        [ProgressHUD dismiss];
        self.navigationController.view.userInteractionEnabled = YES;
        
        recordList = result;
        [self.tableView reloadData];
        
        if (completion){
            completion();
        }
    }];
}

- (void)reloadData
{
    // Reload table data
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        [self loadGestureWithCompletion:^{
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM d, h:mm a"];
            NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
            NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                        forKey:NSForegroundColorAttributeName];
            NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
            self.refreshControl.attributedTitle = attributedTitle;
            
            [self.refreshControl endRefreshing];
        }];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    
    if (alreadyLoaded == NO) {
        alreadyLoaded = YES;
        currentUser = [PFUser currentUser];
        [self loadGestureWithCompletion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName = [NSString stringWithFormat:@"All Gesture:%lu",(unsigned long)recordList.count];
   
    return sectionName;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return recordList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    PFObject *obj=[recordList objectAtIndex:indexPath.row];
    static NSString *CellIdentifier = @"Cell";
    RecordCell *cell = (RecordCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"RecordCell" owner:self options:nil];
        cell = recordCell;
    }
    // Configure the cell...

    PFFile *gestImageFile = [obj objectForKey:@"patternImg"];
   
    [gestImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:imageData];
            cell.gestureImageView.image = image;
        }
    }];
    noOffav = [NSString stringWithFormat:@"%@",[obj objectForKey:@"numberOfFav"]];
    noofsee = [NSString stringWithFormat:@"%@",[obj objectForKey:@"numberOfsee"]];
    
    NSString *createdDate = [NSString stringWithFormat:@"%@",obj.createdAt];
    NSString *end = [NSString stringWithFormat:@"%@",[NSDate date]];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-ddHH:mm:ss ZZZ"];
    NSDate *startDate = [f dateFromString:createdDate];
    NSDate *endDate = [f dateFromString:end];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                             options:0];
    
    NSDateComponents *componentsHours = [gregorianCalendar components:NSHourCalendarUnit
                                                             fromDate:startDate
                                                               toDate:endDate
                                                              options:0];
    NSDateComponents *componentMint = [gregorianCalendar components:NSMinuteCalendarUnit
                                                           fromDate:startDate
                                                             toDate:endDate
                                                            options:0];
    NSDateComponents *componentSec = [gregorianCalendar components:NSSecondCalendarUnit
                                                          fromDate:startDate
                                                            toDate:endDate
                                                           options:0];
    
    // @@@ update negative counter 09/04/15 by andre
    if (components.day == 0) {
        if (componentsHours.hour ==0) {
            if (componentMint.minute ==0) {
                if (componentMint.second ==0) {
                    cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld sec",(long)componentSec.second];
                }else{
                    cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld sec",(long)componentSec.second];
                }

            }else{
                cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld min",(long)componentMint.minute];
            }
            
        }else{
            if (componentsHours.hour > 1) {
                cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld hours",(long)componentsHours.hour];
            }else{
               cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld hour",(long)componentsHours.hour];
            }
        }
        
    }else{
        if (components.day > 1) {
            cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld days",(long)components.day];
        }else{
            cell.noOfDaylable.text = [NSString stringWithFormat:@"%ld day",(long)components.day];
        }
    }
   
    if ([noOffav isEqualToString:@"0"]) {
        cell.favImageView.image = [UIImage imageNamed:@"fav_unselected.png"];
    }else{
       cell.favImageView.image = [UIImage imageNamed:@"fav_Selected.png"];
    }
    if ([noofsee isEqualToString:@"0"]) {
        cell.eyeImageView.image = [UIImage imageNamed:@"eye_unselected.png"];
    }else{
        cell.eyeImageView.image = [UIImage imageNamed:@"eye_selected.png"];
         if ([noofsee integerValue] > 49 && [noofsee integerValue] < 100) {
             cell.certificateImageView.image = [UIImage imageNamed:@"bronzeMedal.png"];
         } else if ([noofsee integerValue] > 99 && [noofsee integerValue] < 200){
             cell.certificateImageView.image = [UIImage imageNamed:@"silverMedal.png"];
         } else if ([noofsee integerValue] > 199){
             cell.certificateImageView.image = [UIImage imageNamed:@"goldMedal.png"];
         }
    }
    if ([[obj objectForKey:@"filetype"] isEqualToString:@"Audio"]) {
        PFFile *audioFile = [obj objectForKey:@"uploadFile"];
        [audioFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
            if(!error){
                
                
                [audioFile performSelectorAsync:@selector(getData) completion:^(id result) {
                        player = [[AVAudioPlayer alloc] initWithData:result error:nil];
                }];
                
//                double timeduration = player.duration;
//                NSNumber *theDouble = [NSNumber numberWithDouble:timeduration];
//                
//                int inputSeconds = [theDouble intValue];
//                int hours =  inputSeconds / 3600;
//                int minutes = ( inputSeconds - hours * 3600 ) / 60;
//                int seconds = inputSeconds - hours * 3600 - minutes * 60;
//                
//                cell.recordTimeLable.text = [NSString stringWithFormat:@"%.2d:%.2d",minutes, seconds];
                
//                cell.recordTimeLable.text = [NSString stringWithFormat:@"%.2f",timeduration];
            }
            
        }];
    }else if ([[obj objectForKey:@"filetype"] isEqualToString:@"video"]){
        PFFile *videoFile = [obj objectForKey:@"uploadFile"];
        [videoFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
            if(!error){
                
                [videoFile performSelectorAsync:@selector(getData) completion:^(id result) {
                    
                    /// display video
                    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    documentsDirectory = [paths objectAtIndex:0];
                    path = [documentsDirectory stringByAppendingPathComponent:@"myMove.mov"];
                    [[NSFileManager defaultManager] createFileAtPath:path contents:result attributes:nil];
                    moveUrl = [NSURL fileURLWithPath:path];
                }];

               
                
               // AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:moveUrl options:nil];
               
//                NSTimeInterval durationInSeconds = 0.0;
//                if (asset)
//                    durationInSeconds = CMTimeGetSeconds(asset.duration);
//
//                NSInteger ti = (NSInteger)durationInSeconds;
//                NSInteger seconds = ti % 60;
//                NSInteger minutes = (ti / 60) % 60;
//                //NSInteger hours = (ti / 3600);
//                cell.recordTimeLable.text = [NSString stringWithFormat:@"%.2d:%.2d",minutes, seconds];

            }
            
        }];
    }else{
       cell.recordTimeLable.text = @"Image";
    }
    cell.noOfEyeLable.text = noofsee;
    cell.noOfFavLable.text =  noOffav;
    
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [ProgressHUD show:@"Loading..."];
  PFObject *obj=[recordList objectAtIndex:indexPath.row];
   PlayGestureVC *play = [[PlayGestureVC alloc] initWithNibName:@"PlayGestureVC" bundle:nil];
    NSString *filetype = [NSString stringWithFormat:@"%@",[obj objectForKey:@"filetype"]];
//    gest.pattern = [obj objectForKey:@"pattern"];
    play.playerType = filetype;
    play.playerID = obj.objectId;
    PFFile *gesturedata = [obj objectForKey:@"patternImg"];
    [gesturedata getDataInBackgroundWithBlock:^(NSData *gestimageData, NSError *error) {
        if (!error) {
            play.gestureimageData = gestimageData;
        }
    }];
    
    if ([filetype isEqualToString:@"Capture Image"]) {
        PFFile *gestImageFile = [obj objectForKey:@"uploadFile"];
        [gestImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
            if (!error) {
                [gestImageFile performSelectorAsync:@selector(getData) completion:^(id imageData) {
                    //                gest.uploadfile = imageData;
                    play.playFile = imageData;
                    [self.navigationController pushViewController:play animated:YES];
                    [ProgressHUD dismiss];
                }];
            }
        }];
        

    }else if([filetype isEqualToString:@"video"]){
        PFFile *videoFile = [obj objectForKey:@"uploadFile"];
        [videoFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
            if(!error){
                
                [videoFile performSelectorAsync:@selector(getData) completion:^(id result) {
                    //    NSURL    *fileURL    =  [NSURL fileURLWithPath:videoFile.url];
                    //                gest.uploadfile = result;
                    //                gest.videoURl = fileURL;
                    play.playFile = result;
                    
                    [ProgressHUD dismiss];
                    [self.navigationController pushViewController:play animated:YES];
                }];
                
            }
            
        }];
    }else{
        PFFile *audioFile = [obj objectForKey:@"uploadFile"];
        [audioFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
            if(!error){
                result = [audioFile getData];
//                gest.uploadfile = result;
                play.playFile = result;
                [ProgressHUD dismiss];
                [self.navigationController pushViewController:play animated:YES];

            }
           
        }];
    }
    if (recordList.count == 0) {
        [ProgressHUD dismiss];
    }
    
 
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
