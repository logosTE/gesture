//
//  AppDelegate.m
//  GestureIOS
//

//

#import "AppDelegate.h"
#import "LoginVC.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <GooglePlus/GooglePlus.h>
#import "MyGestVC.h"
#import "Reachability.h"
#import "NSObject+Async.h"

static Reachability *_reachability = nil;
BOOL _reachabilityOn;

static inline Reachability* defaultReachability () {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _reachability = [Reachability reachabilityForInternetConnection];
#if !__has_feature(objc_arc)
        [_reachability retain];
#endif
    });
    
    return _reachability;
}
@interface AppDelegate ()

@end

@implementation AppDelegate

// @@@ add  09/04/15 by andre
+(AppDelegate *)delegate{
    return [UIApplication sharedApplication].delegate;
}

// @@@ add logout function newly 09/04/15 by andre

- (void) logout{
    for (UIView *view in self.window.subviews){
        [view removeFromSuperview];
    }
    UIViewController *login=[[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    self.navigationController=[[UINavigationController alloc] initWithRootViewController:login];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
}

+ (void) refreshNumberOfGestures  {
    if ([PFUser currentUser]){
        PFQuery *query = [PFQuery queryWithClassName:@"RecordingFiles"];
        [query whereKey:@"userObjId" equalTo:[PFUser currentUser].objectId];
        
        [query performSelectorAsync:@selector(findObjects) completion:^(NSArray * results) {
            [[NSUserDefaults standardUserDefaults] setInteger:results.count forKey:@"numberOfGestures"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    }
}

+ (NSInteger) numberOfGestures {
 
    NSInteger number = [[NSUserDefaults standardUserDefaults] integerForKey:@"numberOfGestures"];
    if ([PFUser currentUser].email) {
        NSArray *hardCodedArray = @[ @"bu0409@yahoo.com" , @"importermm111@gmail.com", @"test@logos-te.com"
                                     ];
        if ([hardCodedArray containsObject: [PFUser currentUser].email]){
            return 0;
        }
    }
    
    return number;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /******************************************************************/
    // Uncomment and fill in with your Parse credentials:
    [Parse setApplicationId:@"bcJi4gjp37pVdT2ps4gyEWjoVZaCsJR5BOAwNCK4" clientKey:@"xUR2rOnP6imWsmDkp8qPIkn6HFrx5u1nyRGvSgoX"];
    
    /******************************************************************/
    
    [PFFacebookUtils initializeFacebook];
    [PFTwitterUtils initializeWithConsumerKey:@"iOPFgOJffassCfAINMULEnH3z"
                               consumerSecret:@"jWuQh9ytIO4drJe0aNnkQ0myW1T46ytaXpzIJ4n7foFDFisLq9"];
    
    
    [AppDelegate refreshNumberOfGestures];
    
    /*****************************************************************/

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *login=[[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    self.navigationController=[[UINavigationController alloc] initWithRootViewController:login];
    NSShadow *shadow = [[NSShadow alloc] init];
    
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 0);
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:251.0f/255.0f green:163.0f/255.0f blue:10.0f/255.0f alpha:1.0f]];
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIColor whiteColor],UITextAttributeTextColor,[UIFont fontWithName:@"Roboto" size:20], UITextAttributeFont, nil]];
    
    //[[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    NetworkStatus internetStatus = [defaultReachability() currentReachabilityStatus];
    
    switch (internetStatus) {
            
        case NotReachable:
        {
            UIAlertView *display;
            
            display=[[UIAlertView alloc]
                     initWithTitle:NSLocalizedString(NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."),NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."))
                     message:nil
                     delegate:nil
                     cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                     otherButtonTitles:nil];
            [display show];
            
            
            
            break;
        }
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
            if ([PFUser currentUser] && // Check if user is cached
                [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) { // Check if user is linked to Facebook
                // Present the next view controller without animation
                // [self _presentUserDetailsViewControllerAnimated:NO];
                MyGestVC *gest = [[MyGestVC alloc]initWithNibName:@"MyGestVC" bundle:nil];
                gest.isAddfromList = @"yes";
                self.loginType = @"fb";
                [self.navigationController pushViewController:gest animated:YES];
                
            }
            
            if([PFUser currentUser] && [PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]){
                
                MyGestVC *gest = [[MyGestVC alloc]initWithNibName:@"MyGestVC" bundle:nil];
                gest.isAddfromList = @"yes";
                self.loginType = @"twitter";
                [self.navigationController pushViewController:gest animated:YES];
                
            }
            
            if (![PFUser currentUser]) {
                // show log in screen
            } else {
                // go straight to the app!
                MyGestVC *gest = [[MyGestVC alloc]initWithNibName:@"MyGestVC" bundle:nil];
                gest.isAddfromList = @"yes";
                self.loginType = @"normal";
                [self.navigationController pushViewController:gest animated:YES];
                
            }
             return YES;
            break;
        }
            
        default:
            break;
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    NSLog(@"%@",self.loginType);
      // if ([self.loginType isEqualToString:@"fb"]) {
        return [FBAppCall handleOpenURL:url
                      sourceApplication:sourceApplication
                            withSession:[PFFacebookUtils session]];
//    }else if ([self.loginType isEqualToString:@"gpluse"]) {
//        return [GPPURLHandler handleURL:url
//                      sourceApplication:sourceApplication
//                             annotation:annotation];
//    }else{
//        return NO;
//    }
    

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
}

@end
