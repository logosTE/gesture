//
//  RecordingListTV.h
//  GestureIOS
//

//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "ProgressHUD.h"
@interface RecordingListTV : UITableViewController

@property (retain, nonatomic) IBOutlet UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
