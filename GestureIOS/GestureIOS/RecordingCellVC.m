//
//  RecordingCellVC.m
//  GestureIOS
//
//  Created by Nirav vavadiya on 23/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import "RecordingCellVC.h"

@interface RecordingCellVC ()

@end

@implementation RecordingCellVC

@synthesize gestureImageView = _gestureImageView;
@synthesize noOfSeeImageView = _noOfSeeImageView;
@synthesize noOfFavImageView = _noOfFavImageView;
@synthesize certificateImageView = _certificateImageView;

@synthesize noOfSeeLable = _noOfSeeLable;
@synthesize noOfFavLable = _noOfFavLable;
@synthesize recordingTimeLable = _recordingTimeLable;
@synthesize gestCell = _gestCell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
