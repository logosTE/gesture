//
//  NSObject+Async.h
//  GestureIOS
//
//  Created on 07/07/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Async)

- (void) performSelectorAsync:(SEL) selector completion:(void(^)(id result))completion;

@end
