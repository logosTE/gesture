//
//  LoginVC.m
//  GestureIOS
//
//  Created by Nirav vavadiya on 12/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import "LoginVC.h"
#import "SignUpVC.h"
#import "MyGestVC.h"
#import "RecordingListTV.h"
#import "Reachability.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <GooglePlus/GooglePlus.h>
static NSString * const kClientId = @"235283447374-baul39a5i12jj3vtvrgsvr1pdgavv4lr.apps.googleusercontent.com";
#import "NSObject+Async.h"

static Reachability *_reachability = nil;
BOOL _reachabilityOn;

static inline Reachability* defaultReachability () {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _reachability = [Reachability reachabilityForInternetConnection];
#if !__has_feature(objc_arc)
        [_reachability retain];
#endif
    });
    
    return _reachability;
}

@interface LoginVC ()

@end

@implementation LoginVC
@synthesize signInButton;
GPPSignIn *signIn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

           }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.emailchkImg.hidden = YES;
    self.pwdchkImg.hidden = YES;
    
    self.navigationItem.title = @"Sign In";
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.translucent = NO;
    returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    [returnKeyHandler setLastTextFieldReturnKeyType:UIReturnKeyDone];
    returnKeyHandler.toolbarManageBehaviour = IQAutoToolbarByPosition;
    
//    self.txtEmail.text = @"test@gmail.com";
//    self.txtpwd.text = @"v123456";
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtpwd.leftView = paddingView;
    self.txtpwd.leftViewMode = UITextFieldViewModeAlways;
    self.txtpwd.layer.borderWidth = 2.0f;
    self.txtpwd.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];
    
    
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtEmail.leftView = paddingView1;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    self.txtEmail.layer.borderWidth = 2.0f;
    self.txtEmail.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];

    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientId;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [signIn trySilentAuthentication];
}

// @@@ add newly 09/04/15 by andre
- (void) showRedBlueProgressView{
    ProgressView = [[UIView alloc] initWithFrame:self.view.bounds];
    ProgressView.backgroundColor = [UIColor grayColor];
    ProgressView.alpha = 0.1;
    [self.view addSubview:ProgressView];
    redBlueView = [[GmailLikeLoadingView alloc] initWithFrame:CGRectMake(135, 150, 50, 50)];
    [self.view addSubview:redBlueView];
    [redBlueView startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// @@@ add newly 09/04/15 by andre
- (void) hideRedBlueProgressView{
    [redBlueView stopAnimating];
    [redBlueView removeFromSuperview];
    [ProgressView removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signUpbutton_click:(id)sender {
    SignUpVC * signup = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:signup animated:YES];
}

- (IBAction)signInButton_click:(id)sender {
    [self Login];
    appDelegate.loginType = @"normal";
    
}
-(void) Login{
    if (([self.txtEmail.text length] > 0) && ([self.txtpwd.text length] > 0))
    {
        NetworkStatus internetStatus = [defaultReachability() currentReachabilityStatus];
        
        switch (internetStatus) {
            case NotReachable:
            {
                UIAlertView *display;
                
                display=[[UIAlertView alloc]
                         initWithTitle:NSLocalizedString(NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."),NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."))
                         message:nil
                         delegate:nil
                         cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                         otherButtonTitles:nil];
                [display show];
                break;
            }
            case ReachableViaWiFi:
            case ReachableViaWWAN:
            {
                
                [ProgressHUD show:@"Loading..."];
                
                //[self Login_Called];
                
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(Login_Called) userInfo:Nil repeats:NO];
                
                break;
            }
                
            default:
                break;
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc ] initWithTitle:@"Error" message:@"Enter correct username and password" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        
        [alert show];
        self.emailchkImg.hidden = NO;
        self.pwdchkImg.hidden = NO;
        [self.emailchkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        [self.pwdchkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        
        
    }
}


-(void) Login_Called
{
    PFQuery *query1 = [PFUser query];
    [query1 whereKey:@"email" equalTo:self.txtEmail.text];
    NSArray *objects1;
    
    [query1 performSelectorAsync:@selector(findObjects) completion:^(id result) {
        self.emailchkImg.hidden = NO;
        self.pwdchkImg.hidden = NO;
        if([objects1 count] > 0 ){
            
            [self.emailchkImg setImage:[UIImage imageNamed:@"right.png"]];
        }else{
            [self.emailchkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        }
        self.emailchkImg.hidden = YES;
        self.pwdchkImg.hidden = YES;
        
        [PFUser logInWithUsernameInBackground:self.txtEmail.text password:self.txtpwd.text block:^(PFUser *user, NSError *error) {
            
            if (user) {

                [AppDelegate refreshNumberOfGestures];
                
                [ProgressHUD showSuccess:@"Login Success"];
                MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
                gest.isAddfromList = @"yes";
                [self.navigationController pushViewController:gest animated:YES];
            } else {
                // The login failed. Check error to see why.
                [ProgressHUD showError:@"Login Failed"];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Invalid" message:@"Invalid user or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                self.pwdchkImg.hidden = NO;
                self.emailchkImg.hidden = NO;
                [self.pwdchkImg setImage:[UIImage imageNamed:@"wrong.png"]];
                [alert show];
            }
        }];
    }];
}
- (IBAction)fbLogin:(id)sender {
    [self showRedBlueProgressView];
    NetworkStatus internetStatus = [defaultReachability() currentReachabilityStatus];
    
    switch (internetStatus) {
            
        case NotReachable:
        {
            UIAlertView *display;
            
            display=[[UIAlertView alloc]
                     initWithTitle:NSLocalizedString(NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."), NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."))
                     message:nil
                     delegate:nil
                     cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                     otherButtonTitles:nil];
            [display show];
            
            [self hideRedBlueProgressView];
            
            break;
        }
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
            [PFFacebookUtils.session closeAndClearTokenInformation];
            [PFUser logOut];
            [PFFacebookUtils initializeFacebook];
            [PFFacebookUtils logInWithPermissions:@[@"public_profile", @"email", @"user_friends"] block:^(PFUser *user, NSError *error) {
                if (!user) {
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    NSLog(@"Uh oh. The user cancelled the Facebook login.");
                    [self hideRedBlueProgressView];
                } else {
                    NSLog(@"User logged in through Facebook!");
    
                    [AppDelegate refreshNumberOfGestures];
                    
                    MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
                    gest.isAddfromList = @"yes";
                    [self.navigationController pushViewController:gest animated:YES];
                    [self hideRedBlueProgressView];
                    appDelegate.loginType = @"fb";
                }
            }];
            
            break;
        }
        default:
            break;
    }
    
}

- (IBAction)forgotPwd:(id)sender {
    if ([self.txtEmail.text length] <=0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter Email." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        [self.emailchkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        return;
        
    }else{
        [self.emailchkImg setImage:[UIImage imageNamed:@"right.png"]];
        [PFUser requestPasswordResetForEmailInBackground:self.txtEmail.text block:^(BOOL succeeded, NSError *error) {
            UIAlertView *display;
            if(succeeded){
                display=[[UIAlertView alloc] initWithTitle:@"Password email" message:@"Please check your email for resetting the password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                
            }else{
                display=[[UIAlertView alloc] initWithTitle:@"Email" message:@"Email doesn't exists in our database" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
            }
            [display show];
        }];
    }
}

- (IBAction)twitterLogin:(id)sender {
    [ProgressHUD show:@"Signing in..."];
    NetworkStatus internetStatus = [defaultReachability() currentReachabilityStatus];
    
    switch (internetStatus) {
            
        case NotReachable:
        {
            UIAlertView *display;
            
            display=[[UIAlertView alloc]
                     initWithTitle:NSLocalizedString(NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."),NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."))
                     message:nil
                     delegate:nil
                     cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                     otherButtonTitles:nil];
            [display show];
            [ProgressHUD dismiss];
            
            
            break;
        }
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
  
            [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
                if (!user) {
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Uh oh. The user cancelled the Twitter login." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    NSLog(@"Uh oh. The user cancelled the Twitter login.");
                    [ProgressHUD dismiss];
                    return;
                } else {
                    NSLog(@"User logged in with Twitter!");
                    MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
                    gest.isAddfromList = @"yes";
                    [self.navigationController pushViewController:gest animated:YES];
                    [ProgressHUD dismiss];
                    appDelegate.loginType = @"twitter";
                    [AppDelegate refreshNumberOfGestures];
                }
     
            }];
            
            break;
        }
            
        default:
            break;
    }
}

- (IBAction)googleLogin:(id)sender {
    appDelegate.loginType = @"gpluse";
   // [signIn trySilentAuthentication];
}

- (void)loginWithGooglePlus
{
    [GPPSignIn sharedInstance].clientID = kClientId;
    [GPPSignIn sharedInstance].scopes= @[ @"profile" ];
    [GPPSignIn sharedInstance].shouldFetchGoogleUserID=YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail=YES;
    [GPPSignIn sharedInstance].delegate=self;
    
    //[[GPPSignIn sharedInstance] authenticate];
}
//- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
//                   error: (NSError *) error {
//    NSLog(@"Received error %@ and auth object %@",error, auth);
//    if (error) {
//        // Do some error handling here.
//    } else {
//        [self refreshInterfaceBasedOnSignIn];
//    }
//}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth  error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    NSLog(@"user email %@  user id %@  user data %@, ",auth.userEmail,auth.userID, auth.userData);
    
    NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %@",[GPPSignIn sharedInstance].authentication.userEmail]);
    NSLog(@"Received error %@ and auth object %@",error, auth);
    
    NSString *userName = [[auth.userEmail componentsSeparatedByString:@"@"] objectAtIndex:0];
    PFUser *user = [PFUser user];
    user.username = userName;
    user.password = @"12345"; //It'll your common password for all google+ users.
    user.email = auth.userEmail;
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"Successfull Registration");
            //Here getting user profile information
            
            GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
            plusService.retryEnabled = YES;
            
            // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
            [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
            
            GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
            
            [plusService executeQuery:query
                    completionHandler:^(GTLServiceTicket *ticket,
                                        GTLPlusPerson *person,
                                        NSError *error) {
                        if (error) {
                            GTMLoggerError(@"Error: %@", error);
                        } else {
                            // Retrieve the display name and "about me" text
                            NSLog(@"Name %@  Display name %@  Person about %@ person birthday %@"  ,person.name,person.displayName,person.aboutMe ,person.image);
                            
                            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:person.image.url]];
                            PFFile *imageFile = [PFFile fileWithName:@"Profileimage.png" data:imageData];
                            
                            PFUser *user = [PFUser currentUser];
                            // other fields can be set just like with PFObject
                            user[@"User_name"] = person.displayName;
                            user[@"user_image"] = imageFile;
                            [user saveInBackground];
                            
                        }
                    }];
            
        } else {
            //If user already register then it'll login
            NSLog(@"error %@",error);
            [PFUser logInWithUsernameInBackground:userName password:@"12345"
                                            block:^(PFUser *user, NSError *error) {
                                                NSLog(@"object id for me = %@", user.objectId);
                                                if (user) {
                                                    NSLog(@"user login success %@",user);
                                                }else{
                                                    NSLog(@"Error on Login %@",error);
                                                }
                                            }];
            // Show the errorString somewhere and let the user try again.
        }
    }];
    
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(void)refreshInterfaceBasedOnSignIn {
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        self.signInButton.hidden = YES;
        // Perform other actions here, such as showing a sign-out button
    } else {
        self.signInButton.hidden = NO;
        // Perform other actions here
    }
}
@end
