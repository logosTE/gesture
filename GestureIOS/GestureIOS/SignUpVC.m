//
//  SignUpVC.m
//  GestureIOS
//

//

#import "SignUpVC.h"
#import "LoginVC.h"
#import "TermsVC.h"
#import "FAQVC.h"
#import "Reachability.h"
#import "MyGestVC.h"
#import "NSObject+Async.h"

static Reachability *_reachability = nil;
BOOL _reachabilityOn;

static inline Reachability* defaultReachability () {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _reachability = [Reachability reachabilityForInternetConnection];
#if !__has_feature(objc_arc)
        [_reachability retain];
#endif
    });
    
    return _reachability;
}

@interface SignUpVC ()

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.navigationItem.title = @"Sign Up";
    
    self.emailChkimg.hidden = YES;
    self.pwdChkImg.hidden = YES;
    self.nameChkImg.hidden = YES;
    self.userIdChkImg.hidden = YES;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtpwd.leftView = paddingView;
    self.txtpwd.leftViewMode = UITextFieldViewModeAlways;
    self.txtpwd.layer.borderWidth = 2.0f;
    self.txtpwd.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtemail.leftView = paddingView2;
    self.txtemail.leftViewMode = UITextFieldViewModeAlways;
    self.txtemail.layer.borderWidth = 2.0f;
    self.txtemail.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];
    
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtName.leftView = paddingView3;
    self.txtName.leftViewMode = UITextFieldViewModeAlways;
    self.txtName.layer.borderWidth = 2.0f;
    self.txtName.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];
    
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 20)];
    self.txtUserID.leftView = paddingView4;
    self.txtUserID.leftViewMode = UITextFieldViewModeAlways;
    self.txtUserID.layer.borderWidth = 2.0f;
    self.txtUserID.layer.borderColor = [[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]CGColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signUpButtonClick:(id)sender {
    
    
//    
//    if ([self.txtName.text length] <=0)
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter User Name." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//        self.nameChkImg.hidden = NO;
//        [self.nameChkImg setImage:[UIImage imageNamed:@"wrong.png"]];
//        return;
//        
//    }
    
    if ([self.txtUserID.text length] <=0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter UserID." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        self.userIdChkImg.hidden = NO;
        [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
        [self.userIdChkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        return;
        
    }
    
    if ([self.txtemail.text length] <=0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter Email." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        self.emailChkimg.hidden = NO;
        [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
        [self.userIdChkImg setImage:[UIImage imageNamed:@"right.png"]];
        [self.emailChkimg setImage:[UIImage imageNamed:@"wrong.png"]];
        return;
        
    }else{
        if([self validateEmail:self.txtemail.text]==NO)
        {
            [self.txtemail becomeFirstResponder];
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please Enter valid email." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            self.emailChkimg.hidden = NO;
            [alert show];
            [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
            [self.userIdChkImg setImage:[UIImage imageNamed:@"right.png"]];
            [self.emailChkimg setImage:[UIImage imageNamed:@"wrong.png"]];
            return;
            
        }
    }
    if ([self.txtpwd.text length] <=0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter Password." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        self.pwdChkImg.hidden = NO;
        [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
        [self.userIdChkImg setImage:[UIImage imageNamed:@"right.png"]];
        [self.emailChkimg setImage:[UIImage imageNamed:@"right.png"]];
        [self.pwdChkImg setImage:[UIImage imageNamed:@"wrong.png"]];
        self.txtpwd.text = @"";
        return;
        
    }else{
        if ([self.txtpwd.text length] <= 6) {
            [[[UIAlertView alloc] initWithTitle:@"Required" message:@"Please Enter Password more than 6 characters." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            self.pwdChkImg.hidden = NO;
            [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
            [self.userIdChkImg setImage:[UIImage imageNamed:@"right.png"]];
            [self.emailChkimg setImage:[UIImage imageNamed:@"right.png"]];
            [self.pwdChkImg setImage:[UIImage imageNamed:@"wrong.png"]];
            self.txtpwd.text = @"";
            return;
        }
    }
    self.userIdChkImg.hidden = YES;
    self.emailChkimg.hidden = YES;
    self.pwdChkImg.hidden = YES;
    self.nameChkImg.hidden = YES;
//    // Submit Data to Server
//    
    NetworkStatus internetStatus = [defaultReachability() currentReachabilityStatus];
    
    switch (internetStatus) {
            
        case NotReachable:
        {
            UIAlertView *display;
            
            display=[[UIAlertView alloc]
                     initWithTitle:NSLocalizedString(NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."),NSLocalizedString(@"Internet connection not found, please check your internet connection and try again.",@"Internet connection not found, please check your internet connection and try again."))
                     message:nil
                     delegate:nil
                     cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                     otherButtonTitles:nil];
            [display show];
            
            
            
            break;
        }
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
            
            
            [ProgressHUD show:@"Creating account..."];
            
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(Submit_Called) userInfo:Nil repeats:NO];
            
            break;
        }
            
        default:
            break;
    }
    
}

-(void) Submit_Called{
    
    PFQuery *query1 = [PFUser query];
    [query1 whereKey:@"email" equalTo:self.txtemail.text];
    
    [query1 performSelectorAsync:@selector(findObjects) completion:^(id objects1) {
        PFQuery *query2 = [PFUser query];
        [query2 whereKey:@"userId" equalTo:self.txtUserID.text];
        
        [query2 performSelectorAsync:@selector(findObjects) completion:^(id objects2) {
            if (([objects1 count] >0) || ([objects2 count] >0)) {
                if ([objects1 count] >0) {
                    
                    
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"User email already exists!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [ProgressHUD dismiss];
                    [alert show];
                    
                    self.emailChkimg.hidden = NO;
                    [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.userIdChkImg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.pwdChkImg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.emailChkimg setImage:[UIImage imageNamed:@"wrong.png"]];
                }
                else
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"UserID already exists!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [ProgressHUD dismiss];
                    [alert show];
                    self.userIdChkImg.hidden = NO;
                    [self.nameChkImg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.emailChkimg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.pwdChkImg setImage:[UIImage imageNamed:@"right.png"]];
                    [self.userIdChkImg setImage:[UIImage imageNamed:@"wrong.png"]];
                    
                }
            }else{
                
                
                PFUser *user = [PFUser user];
                
                user.username = self.txtemail.text;
                user.password = self.txtpwd.text;
                user.email = self.txtemail.text;
                user[@"userId"] = self.txtUserID.text;
                user[@"name"] = self.txtName.text;
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    [ProgressHUD dismiss];
                    
                    if (!error) {
                        
                        
                        //Store data in Appdelegates
                        // Show success message
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Account created, enjoy GestURe!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        alert.tag=3;
                        [alert show];
                        
                        // Dismiss the controller
                        //[self.navigationController popToRootViewControllerAnimated:YES];
                        
                        
                        
                        
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Signup Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                }];
                
            }

        }];
    }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3) {
//        [self.navigationController popViewControllerAnimated:TRUE];
        MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
        [self.navigationController pushViewController:gest animated:YES];

    }
    
    
}
- (IBAction)signInButtonClick:(id)sender {
//    LoginVC *login = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//    [self.navigationController popToViewController:login animated:YES];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)termsBurronClick:(id)sender {
    TermsVC *terms = [[TermsVC alloc] initWithNibName:@"TermsVC" bundle:nil];
    [self.navigationController pushViewController:terms animated:YES];
}

- (IBAction)onDisplayFAQ:(id)sender {
    FAQVC *faq = [[FAQVC alloc] initWithNibName:@"FAQVC" bundle:nil];
    [self.navigationController pushViewController:faq animated:YES];
}

// @@@ add 09/04/15 by andre
-(BOOL)validateEmail:(NSString*)email
{
    if( (0 != [email rangeOfString:@"@"].length) &&  (0 != [email rangeOfString:@"."].length) )
    {
        NSMutableCharacterSet *invalidCharSet = [[[NSCharacterSet alphanumericCharacterSet] invertedSet]mutableCopy];
        [invalidCharSet removeCharactersInString:@"_-"];
        
        NSRange range1 = [email rangeOfString:@"@" options:NSCaseInsensitiveSearch];
        
        // If username part contains any character other than "."  "_" "-"
        NSString *usernamePart = [email substringToIndex:range1.location];
        NSArray *stringsArray1 = [usernamePart componentsSeparatedByString:@"."];
        for (NSString *string in stringsArray1)
        {
            NSRange rangeOfInavlidChars=[string rangeOfCharacterFromSet: invalidCharSet];
            if(rangeOfInavlidChars.length !=0 || [string isEqualToString:@""])
                return NO;
        }
        
        NSString *domainPart = [email substringFromIndex:range1.location+1];
        NSArray *stringsArray2 = [domainPart componentsSeparatedByString:@"."];
        
        for (NSString *string in stringsArray2)
        {
            NSRange rangeOfInavlidChars=[string rangeOfCharacterFromSet:invalidCharSet];
            if(rangeOfInavlidChars.length !=0 || [string isEqualToString:@""])
                return NO;
        }
        
        return YES;
    }
    else
        return NO;
}

//- (BOOL)validateEmail:(NSString *)inputText {
//    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    NSRange aRange;
//    if([emailTest evaluateWithObject:inputText]) {
//        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
//        int indexOfDot = aRange.location;
//        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
//        if(aRange.location != NSNotFound) {
//            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
//            topLevelDomain = [topLevelDomain lowercaseString];
//            //NSLog(@"topleveldomains:%@",topLevelDomain);
//            NSSet *TLD;
//            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
//            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
//                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
//                return TRUE;
//            }
//            /*else {
//             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
//             }*/
//            
//        }
//    }
//    return FALSE;
//}
@end
