//
//  RecordingGestureVC.m
//  GestureIOS
//
//

#import "RecordingGestureVC.h"
#import "RecordingListTV.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <Parse/Parse.h>
#import "AVCamPreviewView.h"
#import "ProgressHUD.h"
#import "MyGestVC.h"
#import <FacebookSDK/FacebookSDK.h>

static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface RecordingGestureVC ()<AVCaptureFileOutputRecordingDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate,UIImagePickerControllerDelegate>{
    NSString *recordType;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSTimer *timer;
    UIButton *cameraShoot;
    UIButton *video_startButton;
    BOOL isvideo_start;
    UIImage *sendimage;
    NSData *sendImageData;
     NSData *sendVideoData;
    UILabel *videoCounterLable;
    MPMoviePlayerController *moviePlayer;
    double videoSize;
    PFUser *currentUser;
    NSURL *AudioRePlayurl;
    NSURL *videoRePlayurl;
    
    __block BOOL recording;
}

@property (nonatomic, weak) IBOutlet AVCamPreviewView *previewView;

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer;

// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;

@end

@implementation RecordingGestureVC


- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    self.flipCameraButton.hidden = YES;
     [self clearTmpDirectory];
    currentUser = [PFUser currentUser];

    [self cameraConfiguration];
    [self cameraViewWillAppear];
  
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    // NSLog(@"%d",randomNumber);
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               [NSString stringWithFormat: @"MyAudioMemo%@.m4a",timestamp],
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
    [recordSetting setValue: [NSNumber numberWithInt:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVEncoderBitRateKey];
    
    
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    
    [self onCameraButtonClick];

   
}
- (void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

// @@@ add by andre
#pragma mark - customAlertView Method

- (void) showAlertView:(id)title{
    self.lblTitle.text = title;
    self.opaqueView.hidden = NO;
    self.alertView.hidden = NO;
    [self.view bringSubviewToFront:self.opaqueView];
    [self.view bringSubviewToFront:self.alertView];
}

- (void) hideAlertView{
    self.opaqueView.hidden = YES;
    self.alertView.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.alertView.layer.cornerRadius = 5;
    self.alertView.clipsToBounds = YES;
    [self hideAlertView];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    currentUser = [PFUser currentUser];
    
    sendVideoData = [[NSData alloc]init];
    recordType = @"Audio";
    self.Audio_MicImageview.hidden = NO;
    self.audio_Lable.hidden = NO;
    self.audio_button.hidden = NO;
    self.flipCameraButton.hidden = YES;
    
    //camera shoot button8***************************************
    cameraShoot = nil;
    cameraShoot  = [UIButton buttonWithType:UIButtonTypeSystem];
    cameraShoot.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -190 , [[UIScreen mainScreen]bounds].size.height - 130, 67, 67);
    [cameraShoot setBackgroundImage:[UIImage imageNamed:@"camera_shoot.png"] forState:UIControlStateNormal];
    cameraShoot.hidden = YES;
    [cameraShoot addTarget:self action:@selector(btnCameraShoot:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cameraShoot];
    
    /***************************************************************/
    //video_startButton
    video_startButton = nil;
    isvideo_start = NO;
    video_startButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    video_startButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -190 , [[UIScreen mainScreen]bounds].size.height - 130, 67, 67);
    [video_startButton setBackgroundImage:[UIImage imageNamed:@"video_start.png"] forState:UIControlStateNormal];
    video_startButton.hidden = YES;
    [video_startButton addTarget:self action:@selector(btnVideoRecordingStart:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:video_startButton];
    
     /***************************************************************/
        //video Start Label
    videoCounterLable = nil;
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Neue" size:17];
    videoCounterLable = [[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width - 270 , [[UIScreen mainScreen]bounds].size.height - 130, 67, 67)];
    videoCounterLable.textColor = [UIColor whiteColor];
    videoCounterLable.font = customFont;
    videoCounterLable.hidden = YES;
    videoCounterLable.text = @"00:00";
    [self.view addSubview:videoCounterLable];
    
    /***************************************************************/
    
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    [self.bg_imageview setImage:[UIImage imageNamed:@"audio_bg.png"]];
    btnExit  = [UIButton buttonWithType:UIButtonTypeSystem];
    btnExit.frame = CGRectMake(275 , 23, 32, 32);
    [btnExit setBackgroundImage:[UIImage imageNamed:@"exit@2x.png"] forState:UIControlStateNormal];
    btnExit.hidden = YES;
    [btnExit addTarget:self action:@selector(onExit) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.opaqueView];
    [self.view addSubview:self.alertView];
}

/****************** Camera Configuration **********************/
-(void) cameraConfiguration{
   
    // Create the AVCaptureSession
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    [self setSession:session];
    
    // Setup the preview view
    [[self previewView] setSession:session];
    
    // Check for device authorization
    [self checkDeviceAuthorizationStatus];
    
    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
    
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];
    
    dispatch_async(sessionQueue, ^{
        [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [RecordingGestureVC deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];

        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
     
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                
                [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)[self interfaceOrientation]];
            });
        }
        
        AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];

        AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];

        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:audioDeviceInput])
        {
            [session addInput:audioDeviceInput];
            
        }

        AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
       
        if ([session canAddOutput:movieFileOutput])
        {
            [session addOutput:movieFileOutput];
            AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
          
            if ([connection isVideoStabilizationSupported])
                //[connection setEnablesVideoStabilizationWhenAvailable:YES];// @@@ update by andre
                [connection setPreferredVideoStabilizationMode:AVCaptureVideoStabilizationModeAuto];
            
            
            [self setMovieFileOutput:movieFileOutput];
        }
        
        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([session canAddOutput:stillImageOutput])
        {
            [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
        
            [session addOutput:stillImageOutput];
            [self setStillImageOutput:stillImageOutput];
        }
    });
}


-(void) cameraViewWillAppear{
    
    sec = 00;
    min = 00;
    
    dispatch_async([self sessionQueue], ^{
        [self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
        [self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
        [self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        
        __weak RecordingGestureVC *weakSelf = self;
        [self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
            RecordingGestureVC *strongSelf = weakSelf;
            dispatch_async([strongSelf sessionQueue], ^{
                // Manually restarting the session since it must have been stopped due to an error.
                [[strongSelf session] startRunning];
               // [[strongSelf recordButton]setTitle:NSLocalizedString(@"Record", @"Recording button record title") forState:UIControlStateNormal];
                [video_startButton setBackgroundImage:[UIImage imageNamed:@"video_start.png"] forState:UIControlStateNormal];
                
            });
        }]];
        [[self session] startRunning];
    });
}

-(void) cameraviewWillDisappear{
    
    dispatch_async([self sessionQueue], ^{
        [[self session] stopRunning];

        for(AVCaptureInput *input in _session.inputs) {
            [_session removeInput:input];
        }
        
        for(AVCaptureOutput *output in _session.outputs) {
            [_session removeOutput:output];
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
        
        [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
        [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
        [self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
    });
}
-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    if ([recordType isEqualToString:@"video"]) {
        dispatch_async([self sessionQueue], ^{
            [[self session] stopRunning];
            for(AVCaptureInput *input in _session.inputs) {
                [_session removeInput:input];
            }
            
            for(AVCaptureOutput *output in _session.outputs) {
                [_session removeOutput:output];
            }
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
            [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
            
            [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
            [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
            [self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
        });
    }else if([recordType isEqualToString:@"Capture Image"]){
        dispatch_async([self sessionQueue], ^{
            [[self session] stopRunning];
            for(AVCaptureInput *input in _session.inputs) {
                [_session removeInput:input];
            }
            
            for(AVCaptureOutput *output in _session.outputs) {
                [_session removeOutput:output];
            }
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
            [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
            
            [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
            [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
            [self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
        });
    }
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    // Disable autorotation of the interface when recording is in progress.
    return ![self lockInterfaceRotation];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)toInterfaceOrientation];
}

- (void) recordingDidStart
{
    __block BOOL alertThrown = false;
    recording = true;
    
//    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    for (id subview in self.previewView.subviews){
        if (subview != video_startButton) {
            [subview setUserInteractionEnabled:NO];
        }
    }
    
    dispatch_async([self sessionQueue], ^{ // Background task started
        while ([[self movieFileOutput] isRecording]) {
            double duration = CMTimeGetSeconds([[self movieFileOutput] recordedDuration]);
            
            dispatch_async(dispatch_get_main_queue(), ^{ // Here I dispatch to main queue and update the progress view.

                int seconds = ceil(duration);
                
                if (seconds < 8) {
                    videoCounterLable.text = [NSString stringWithFormat:@"00:%02d", seconds];
                }
                
                if (duration >= 7) {
                    
                    //NSLog(@"---> %d ::: %f", seconds, duration);
                    
                    if (alertThrown == false){
                        alertThrown = true;
                        [[[UIAlertView alloc] initWithTitle:@"Warning!"
                                                    message:@"Video file cannot be longer than 7 seconds. Please choose another or shorten the existing one."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                        
                        // stop recording
                        [[self movieFileOutput] stopRecording];
                        [_session stopRunning];
                    }
                }
            });
        }
    });
}

- (void) recordingDidStop
{
    recording = false;
    [ProgressHUD show:@"Processing video..."];
//    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    for (id subview in self.previewView.subviews){
        if (subview != video_startButton) {
            [subview setUserInteractionEnabled:YES];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == CapturingStillImageContext)
    {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
        
        if (isCapturingStillImage)
        {
            [self runStillImageCaptureAnimation];
        }
    }
    else if (context == RecordingContext)
    {
        BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
       
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRecording)
            {
                
                NSLog(@"start ::: ");
            }
            else
            {
                
                NSLog(@"stop ::: ");
            }
            
            isRecording ? [self recordingDidStart] : [self recordingDidStop];
        });
    }
    else if (context == SessionRunningAndDeviceAuthorizedContext)
    {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRunning)
            {
                cameraShoot.enabled = YES;
                video_startButton.enabled = YES;
            }
            else
            {
                cameraShoot.enabled = NO;
                video_startButton.enabled = NO;
            }
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark Actions
NSString *outputFilePath;

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)[[self previewView] layer] captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:[gestureRecognizer view]]];
    [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

#pragma mark File Output Delegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    if (error)
        NSLog(@"%@", error);
    
    [self setLockInterfaceRotation:NO];
    
    // Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
    UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
    [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
   
    [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:outputFileURL completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error)
            NSLog(@"%@", error);
        
         [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(videoAlertmsg) userInfo:nil repeats:NO];
        [self convertVideoToMediumQuailtyWithInputURL:assetURL outputURL:outputFileURL handler:^(AVAssetExportSession *exportSession)
         {
             if (exportSession.status == AVAssetExportSessionStatusCompleted)
             {
                 [ProgressHUD dismiss];
                 
                 printf("completed\n");
                 sendVideoData = [NSData dataWithContentsOfURL:exportSession.outputURL];
                 NSLog(@"exportSession.outputURL=%@", exportSession.outputURL);
                 videoSize = sendVideoData.length/(1024*1024);
                 NSLog(@"video size:%lu kB",sendVideoData.length/1024);
                // [self videoAlertmsg];
             }
             else
             {
                 printf("error\n");
                 
             }
         }];

        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
        
        if (backgroundRecordingID != UIBackgroundTaskInvalid)
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
    }];
}
- (void)convertVideoToMediumQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
//    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.outputFileType= AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}

#pragma mark Device Configuration

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [[self videoDeviceInput] device];
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    });
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    }
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

#pragma mark UI

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[self previewView] layer] setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [[[self previewView] layer] setOpacity:1.0];
        }];
    });
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted)
        {
            //Granted access to mediaType
            [self setDeviceAuthorized:YES];
        }
        else
        {
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"AVCam!"
                                            message:@"AVCam doesn't have permission to use Camera, please change privacy settings"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}

/******************************************************************/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
int sec = 00;
int min = 00;
-(void)initTimer
{
    sec = 00;
    min = 00;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
}

-(void)startTimer
{
    if ([recordType isEqual:@"video"]) {
        [self stopTimer];
        return;
    }
    
    sec++;
    self.audio_Lable.text = [NSString stringWithFormat:@"%02d : %02d",min,sec];
    videoCounterLable.text = [NSString stringWithFormat:@"%02d : %02d",min,sec];
    
    if (sec == 8) {
        
        [self stopTimer];
        
        if ([recordType isEqualToString:@"video"]) {
            [[self movieFileOutput] stopRecording];
            [_session stopRunning];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:@"Video file cannot be longer than 7 seconds. Please choose another or shorten the existing one."
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert setTag:2];
            
        }
//        else{
//            [recorder stop];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnning!"
//                                                            message:@"Audio not record more than 7 seconds."
//                                                           delegate:self
//                                                  cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
//             [alert setTag:2];
//            NSURL *filePath  = recorder.url;
//            Audiodata = [NSData dataWithContentsOfURL:filePath];
//            
//        }
//        
    }

}
-(void)stopTimer
{
    if (timer) {
        [timer invalidate];
    }
}

/****************************Image capture****************************/

- (IBAction)cameraButton_click:(id)sender {
//    recordType = @"Capture Image";
//    self.bg_imageview.hidden = YES;
//    [self.bg_imageview setImage:[UIImage imageNamed:@"imageRecord.png"]];
//    [self.cameraButton setImage:[UIImage imageNamed:@"camera_selected.png"] forState:UIControlStateNormal];
//    [self.audioButton setImage:[UIImage imageNamed:@"voice_Unselected.png"] forState:UIControlStateNormal];
//    [self.videoButton setImage:[UIImage imageNamed:@"video_unselected.png"] forState:UIControlStateNormal];
//    self.Audio_MicImageview.hidden = YES;
//    self.audio_Lable.hidden = YES;
//    self.audio_button.hidden = YES;
//    video_startButton.hidden = YES;
//    self.flipCameraButton.hidden = NO;
//    cameraShoot.hidden = NO;
//    videoCounterLable.hidden = YES;
    [self onCameraButtonClick];
}
- (void) onCameraButtonClick
{
    recordType = @"Capture Image";
    self.bg_imageview.hidden = YES;
    [self.bg_imageview setImage:[UIImage imageNamed:@"imageRecord.png"]];
    [self.cameraButton setImage:[UIImage imageNamed:@"camera_selected.png"] forState:UIControlStateNormal];
//    [self.audioButton setImage:[UIImage imageNamed:@"voice_Unselected.png"] forState:UIControlStateNormal];
    [self.videoButton setImage:[UIImage imageNamed:@"video_unselected.png"] forState:UIControlStateNormal];
    self.Audio_MicImageview.hidden = YES;
    self.audio_Lable.hidden = YES;
    self.audio_button.hidden = YES;
    video_startButton.hidden = YES;
    self.flipCameraButton.hidden = NO;
    cameraShoot.hidden = NO;
    videoCounterLable.hidden = YES;
}

-(void) btnCameraShoot :(id)sender {
    
    dispatch_async([self sessionQueue], ^{
        // Update the orientation on the still image output video connection before capturing.
        [[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
        
        // Flash set to Auto for Still Capture
        [RecordingGestureVC setFlashMode:AVCaptureFlashModeAuto forDevice:[[self videoDeviceInput] device]];
        
        // Capture a still image.
        [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            
            if (imageDataSampleBuffer)
            {
                sendImageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
               
                sendimage = [[UIImage alloc] initWithData:sendImageData];
                [[[ALAssetsLibrary alloc] init] writeImageToSavedPhotosAlbum:[sendimage CGImage] orientation:(ALAssetOrientation)[sendimage imageOrientation] completionBlock:nil];
                
                [self.bg_imageview setImage:sendimage];
                self.bg_imageview.hidden = NO;
                [self showAlertView:@"Send Image ?"];
            }
        }];
    });
}

- (IBAction)flipCamera_click:(id)sender {
    
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *currentVideoDevice = [[self videoDeviceInput] device];
        AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
        AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
        
        switch (currentPosition)
        {
            case AVCaptureDevicePositionUnspecified:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
            case AVCaptureDevicePositionBack:
                preferredPosition = AVCaptureDevicePositionFront;
                break;
            case AVCaptureDevicePositionFront:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
        }
        
        AVCaptureDevice *videoDevice = [RecordingGestureVC deviceWithMediaType:AVMediaTypeVideo preferringPosition:preferredPosition];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
        
        [[self session] beginConfiguration];
        
        [[self session] removeInput:[self videoDeviceInput]];
        if ([[self session] canAddInput:videoDeviceInput])
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:currentVideoDevice];
            
            [RecordingGestureVC setFlashMode:AVCaptureFlashModeAuto forDevice:videoDevice];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:videoDevice];
            
            [[self session] addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
        }
        else
        {
            [[self session] addInput:[self videoDeviceInput]];
        }
        
        [[self session] commitConfiguration];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
}

/****************************Audio player****************************/


- (IBAction)audioButton_click:(id)sender {
    recordType = @"Audio";
    self.bg_imageview.hidden = NO;
    self.flipCameraButton.hidden = YES;
    [self.bg_imageview setImage:[UIImage imageNamed:@"audio_bg.png"]];
//    [self.audioButton setImage:[UIImage imageNamed:@"voiceSelected.png"] forState:UIControlStateNormal];
    [self.cameraButton setImage:[UIImage imageNamed:@"camera_unselected.png"] forState:UIControlStateNormal];
    [self.videoButton setImage:[UIImage imageNamed:@"video_unselected.png"] forState:UIControlStateNormal];
    self.Audio_MicImageview.hidden = NO;
    self.audio_Lable.hidden = NO;
    self.audio_button.hidden = NO;
    video_startButton.hidden = YES;
    cameraShoot.hidden = YES;
    videoCounterLable.hidden = YES;
}

- (IBAction)audio_start_Click:(id)sender {
    
    [recorder prepareToRecord];
    if ([self.audio_button.titleLabel.text isEqualToString:@"Tab to start"]) {
        [self initTimer];
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        // Start recording
        [recorder record];
        
        [self.audio_button setTitle:@"Tab to stop" forState:UIControlStateNormal];
        
    }
//    else{
//        [recorder stop];
//        [self stopTimer];
////        NSLog(@"Recording url >> %@",recorder.url);
//        [self.audio_button setTitle:@"Tab to start" forState:UIControlStateNormal];
//        [self showAlertView:@"Send Audio ?"];
//        NSURL *filePath  = recorder.url;
//        Audiodata = [NSData dataWithContentsOfURL:filePath];
//    }
}

/****************************Vedio player****************************/

- (IBAction)videoButton_click:(id)sender {
    recordType = @"video";
    
    self.bg_imageview.hidden = YES;
    isvideo_start = YES;
    [self.bg_imageview setImage:[UIImage imageNamed:@"imageRecord.png"]];
    [self.videoButton setImage:[UIImage imageNamed:@"video_selected.png"] forState:UIControlStateNormal];
//    [self.audioButton setImage:[UIImage imageNamed:@"voice_Unselected.png"] forState:UIControlStateNormal];
    [self.cameraButton setImage:[UIImage imageNamed:@"camera_unselected.png"] forState:UIControlStateNormal];
    self.Audio_MicImageview.hidden = YES;
    self.audio_Lable.hidden = YES;
    self.audio_button.hidden = YES;
    cameraShoot.hidden = YES;
    self.flipCameraButton.hidden = NO;
    video_startButton.hidden = NO;
    videoCounterLable.hidden = NO;
    
}

- (void) btnTap {
    [self btnVideoRecordingStart:nil];
}

-(void) btnVideoRecordingStart:(id)sender{
    
   
    if (isvideo_start == YES) {
        
        sec = 0;
        //[self initTimer];
        [video_startButton setBackgroundImage:[UIImage imageNamed:@"video_stop.png"] forState:UIControlStateNormal];
        isvideo_start = NO;
    }else{
        
        //[self stopTimer];
        [video_startButton setBackgroundImage:[UIImage imageNamed:@"video_start.png"] forState:UIControlStateNormal];
        isvideo_start = YES;
        [[self movieFileOutput] stopRecording];
        
    }
    
    dispatch_async([self sessionQueue], ^{
        if (!recording)
        {
            
            [self setLockInterfaceRotation:YES];
            
            if ([[UIDevice currentDevice] isMultitaskingSupported])
            {
                // Setup background task. This is needed because the captureOutput:didFinishRecordingToOutputFileAtURL: callback is not received until AVCam returns to the foreground unless you request background execution time. This also ensures that there will be time to write the file to the assets library when AVCam is backgrounded. To conclude this background execution, -endBackgroundTask is called in -recorder:recordingDidFinishToOutputFileURL:error: after the recorded file has been saved.
                [self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
            }
            
            // Update the orientation on the movie file output video connection before starting recording.
            [[[self movieFileOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
            
            // Turning OFF flash for video recording
            [RecordingGestureVC setFlashMode:AVCaptureFlashModeOff forDevice:[[self videoDeviceInput] device]];

            // Start recording to a temporary file.
            outputFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[@"movie" stringByAppendingPathExtension:@"mov"]];

            [[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:self];
        }
        else
        {
            [[self movieFileOutput] stopRecording];

        }
    });

}


// @@@ add 09/04/15 by andre

- (void) onExit{
    
    [btnExit removeFromSuperview];
    
    if ([recordType isEqualToString:@"Capture Image"]){
        [self imageresubmitButton_Click];
    }
    
    self.bg_imageview.hidden = YES;
    self.cameraButton.hidden = NO;
    self.videoButton.hidden = NO;
    self.gellaryButton.hidden = NO;
    self.flipCameraButton.hidden = NO;
    self.exitButton.hidden = NO;
    cameraShoot.hidden = NO;
}

-(void)imageresubmitButton_Click{
    [self showAlertView:@"Send Image ?"];
}

-(void)onStop{
    [self videoAlertmsg];
}

-(void) videoAlertmsg{
    [ProgressHUD dismiss];
    [moviePlayer.view removeFromSuperview];
    [self showAlertView:@"Send Video ?"];
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [self stopTimer];
    [self showAlertView:@"Send Audio ?"];
}

-(void) recordSubmitButton_Click {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *patternstring = [defaults objectForKey:@"patternstring"];
    
    [ProgressHUD show:@"Uploading..."];
    PFObject *recObject = [PFObject objectWithClassName:@"RecordingFiles"];
    PFFile *patternimageFile = [PFFile fileWithName:@"patternimage.png" data:appDelegate.PatternImage];
    recObject[@"filetype"] = recordType;
    recObject[@"pattern"] = appDelegate.pattern;
    recObject[@"patternImg"] = patternimageFile;
    recObject[@"userObjId"] = currentUser.objectId;
    recObject[@"numberOfFav"] = @0;
    recObject[@"numberOfsee"] = @0;
    recObject[@"patternstring"] = patternstring;
    NSArray *patternArrayCount = [patternstring componentsSeparatedByString:@","];
    NSUInteger patterncount = [patternArrayCount count];
    recObject[@"patterncount"] = [NSNumber numberWithInteger:patterncount];
    if ([recordType isEqualToString:@"Capture Image"])
    {
       PFFile *imageFile = [PFFile fileWithName:@"image.png" data:sendImageData];
        
       recObject[@"uploadFile"] = imageFile;
    }
    else if ([recordType isEqualToString:@"video"])
    {
        PFFile *videoFile = [PFFile fileWithName:@"movie.mp4" data:sendVideoData];
        recObject[@"uploadFile"] = videoFile;
   
//        NSLog(@"audioData = %@", data);
//        NSLog(@"Audio size:%u MB",Audiodata.length/(1024*1024));
//        if(Audiodata.length/(1024*1024) >10){
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Video size less then 10 MB." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
//            [ProgressHUD dismiss];
//            return;
//        }
        //create audiofile as a property
//        PFFile *audioFile = [PFFile fileWithName:@"Memo.m4a" data:Audiodata];
//        recObject[@"uploadFile"] = audioFile;
    }
        //save
        [recObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [ProgressHUD dismiss];
                [cameraShoot setEnabled:YES];
                 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Done" message:@"File successfully uploaded. " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                            [alert show];
                self.bg_imageview.hidden = NO;

                MyGestVC *gest = [[MyGestVC alloc] initWithNibName:@"MyGestVC" bundle:nil];
                gest.isAddfromList  = @"yes";
                [self.navigationController pushViewController:gest animated:YES];
                
                [AppDelegate refreshNumberOfGestures];
            }
        }];
}

- (IBAction)Exit_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)gellaryButton_click:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [imagePicker setDelegate:self];
    imagePicker.allowsEditing = NO;
    if ([recordType isEqualToString:@"Capture Image"]) {

        [self presentViewController:imagePicker animated: YES completion:nil];
        
    }else if ([recordType isEqualToString:@"Audio"]){
        
        MPMediaPickerController *pickerController = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
        pickerController.prompt = @"Select Audio";
        [self presentViewController:pickerController animated:YES completion:nil];
        
    }else{

        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
         [self presentViewController:imagePicker animated: YES completion:nil];
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if ([recordType isEqualToString:@"Capture Image"]){
        UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [self.bg_imageview setImage:image];
        
        self.bg_imageview.contentMode = UIViewContentModeScaleAspectFit;
        
        self.tabBarController.selectedViewController=self;
//        sendImageData = UIImagePNGRepresentation(image);
        sendImageData = UIImageJPEGRepresentation(image, 0.5);
        [self showAlertView:@"Send Image ?"];
    }else{
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
            CMTime audioDuration = sourceAsset.duration;
            int audioDurationSeconds = CMTimeGetSeconds(audioDuration);
            
            if (audioDurationSeconds > 7) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                                message:@"Video file cannot be longer than 7 seconds. Please choose another or shorten the existing one."
                                                               delegate:self
                                                      cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [ProgressHUD dismiss];
            }else{
                
                NSString *moviePath = [videoUrl path];
             
                NSLog(@"%@",videoUrl);
                videoRePlayurl = videoUrl;
                sendVideoData = [NSData dataWithContentsOfURL:videoUrl];
                videoSize = sendVideoData.length/(1024*1024);
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    //UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                    
                }
                [self showAlertView:@"Send Video ?"];
                
            }
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    MPMediaItem *theChosenSong = [[mediaItemCollection items]objectAtIndex:0];
    [self mediaItemToData:theChosenSong];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)mediaItemToData : (MPMediaItem * ) curItem
{
    [ProgressHUD show:@"Loading..."];
    NSURL *url = [curItem valueForProperty: MPMediaItemPropertyAssetURL];
    
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL: url options:nil];
    CMTime audioDuration = songAsset.duration;
    int audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    
    if (audioDurationSeconds > 7) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnning!"
                                                            message:@"Audio file cannot more than 7 seconds."
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [ProgressHUD dismiss];
    }else{
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset: songAsset presetName:AVAssetExportPresetAppleM4A];
        
        exporter.outputFileType =   @"com.apple.m4a-audio";
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * myDocumentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        
        [[NSDate date] timeIntervalSince1970];
        NSTimeInterval seconds = [[NSDate date] timeIntervalSince1970];
        NSString *intervalSeconds = [NSString stringWithFormat:@"%0.0f",seconds];
        
        
        NSString * fileName = [NSString stringWithFormat:@"%@.m4a",intervalSeconds];
        
        NSString *exportFile = [myDocumentsDirectory stringByAppendingPathComponent:fileName];
        
        NSURL *exportURL = [NSURL fileURLWithPath:exportFile];
        exporter.outputURL = exportURL;
        
        // do the export
        // (completion handler block omitted)
        [exporter exportAsynchronouslyWithCompletionHandler:
         ^{
             int exportStatus = exporter.status;
             
             switch (exportStatus)
             {
                 case AVAssetExportSessionStatusFailed:
                 {
                     NSError *exportError = exporter.error;
                     NSLog (@"AVAssetExportSessionStatusFailed: %@", exportError);
                     break;
                 }
                     
                 case AVAssetExportSessionStatusCompleted:
                 {
                     NSLog (@"AVAssetExportSessionStatusCompleted");
                     AudioRePlayurl = exporter.outputURL;
                     
//                     Audiodata = [NSData dataWithContentsOfURL:exporter.outputURL];
                     //Audiodata = [NSData dataWithContentsOfFile: [myDocumentsDirectory stringByAppendingPathComponent:fileName]];
//                     NSLog(@"Audio size:%lu MB",Audiodata.length/(1024*1024));

                     [self showAlertView:@"Send Audio ?"];
                     
                     break;
                 }
                 case AVAssetExportSessionStatusUnknown:
                 {
                     NSLog (@"AVAssetExportSessionStatusUnknown"); break;
                 }
                 case AVAssetExportSessionStatusExporting:
                 {
                     NSLog (@"AVAssetExportSessionStatusExporting"); break;
                 }
                 case AVAssetExportSessionStatusCancelled:
                 {
                     NSLog (@"AVAssetExportSessionStatusCancelled"); break;
                 }
                 case AVAssetExportSessionStatusWaiting:
                 {
                     NSLog (@"AVAssetExportSessionStatusWaiting"); break;
                 }
                 default:
                 {
                     NSLog (@"didn't get export status"); break;
                 }
             }
         }];
    }
}
-(void) mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// @@@ add by andre
#pragma mark - Button Action

- (IBAction)onSubmit:(id)sender {
    NSLog(@"Submit");
    [self hideAlertView];
    [ProgressHUD show:@"Uploading..."];
    
    if ([recordType isEqualToString:@"Capture Image"]) {
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
        [cameraShoot setEnabled:NO];
    }else if ([recordType isEqualToString:@"video"]){
        
        if (videoSize <= 10) {
            [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
        }else{
            [ProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"                    message:@"Video size less then 10 MB." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
    }
}

- (IBAction)onSubmitPost:(id)sender {
    NSLog(@"Post");
    [self hideAlertView];
    [ProgressHUD show:@"Uploading..."];
    
    if ([recordType isEqualToString:@"Capture Image"]) {
       
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
        
        [cameraShoot setEnabled:NO];
        
    }else if ([recordType isEqualToString:@"video"]){
        
        if (videoSize <= 10) {
            [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
        }else{
            [ProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"  message:@"Video size less then 10 MB." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordSubmitButton_Click) userInfo:nil repeats:NO];
    }
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:@"GestURe"]; //the message you want to post
        [mySLComposerSheet addImage:[UIImage imageWithData:appDelegate.PatternImage]];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everything worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
//    {
////        NSData* imageData = UIImageJPEGRepresentation(appDelegate.PatternImage, 90);
//        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                        @"This is my drawing!", @"message",
//                                        appDelegate.PatternImage, @"source",
//                                        nil];
//        
//        [FBRequestConnection startWithGraphPath:@"me/photos"
//                                     parameters:params
//                                     HTTPMethod:@"POST"
//                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                  if (error) {
//                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Post failed!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                                      [alert show];
//                                  }
//                                  else
//                                  {
//                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Posted Successfully!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                                      [alert show];
//                                  }
//                                  
//                              }];
//    }
}

- (IBAction)onReplay:(id)sender {
    NSLog(@"Replay");
    
    [self hideAlertView];
    [ProgressHUD dismiss];
    
    if ([recordType isEqualToString:@"Capture Image"]) {
        
        self.bg_imageview.hidden = NO;
        btnExit.hidden = NO;
        [self.view addSubview:btnExit];
        
        self.cameraButton.hidden = YES;
        self.videoButton.hidden = YES;
        self.gellaryButton.hidden = YES;
        self.flipCameraButton.hidden = YES;
        self.exitButton.hidden = YES;
        cameraShoot.hidden = YES;
        
    }else if ([recordType isEqualToString:@"video"]){
        
        self.flipCameraButton.hidden = YES;
        videoCounterLable.hidden = YES;
        video_startButton .hidden = YES;
        moviePlayer = [[MPMoviePlayerController alloc]init];
        
        if (videoRePlayurl != nil) {
            NSArray *paths;
            NSString *documentsDirectory ;
            NSString *path;
            NSURL *moveUrl;
            paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
            path = [documentsDirectory stringByAppendingPathComponent:@"replayMove.mov"];
            [[NSFileManager defaultManager] createFileAtPath:path contents:sendVideoData attributes:nil];
            moveUrl = [NSURL fileURLWithPath:path];
            [moviePlayer setContentURL:moveUrl];
            
        }else{
            [moviePlayer setContentURL:[NSURL fileURLWithPath:outputFilePath]];
        }
        moviePlayer.view.frame = self.previewView.bounds;
        [self.previewView addSubview:moviePlayer.view];
        [moviePlayer play];
        //        [playerview setFullscreen:YES animated:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onStop)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayer];
        videoCounterLable.text = [NSString stringWithFormat:@"%02d : %02d",min,sec];
        
    }else{
        self.audio_Lable.text = [NSString stringWithFormat:@"%02d : %02d",min,sec];

        [self initTimer];
        
        if (!recorder.recording){
            if (AudioRePlayurl != nil) {
                player = [[AVAudioPlayer alloc] initWithContentsOfURL:AudioRePlayurl error:nil];
            }else{
                player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
            }
            
            player.volume = 16;
            [player setDelegate:self];
            [player play];
        }
        
        [self.audio_button setTitle:@"Playing Audio" forState:UIControlStateNormal];
    }
}

- (IBAction)onCancel:(id)sender {
    NSLog(@"Cancel");
    
    [self hideAlertView];
    self.bg_imageview.hidden = YES;
    
    
    if ([recordType isEqualToString:@"video"]) {
        
        [video_startButton setHidden:false];
        
        [_session startRunning];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath]){
            
            NSError * error = nil;
            
            if ([[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:&error]){
                NSLog(@"---> DELETED: %@", outputFilePath);
            } else {
                NSLog(@"---> ERROR WHILE DELETING (%@) \n: %@", outputFilePath, error);
            }
        }
        
        outputFilePath = nil;
        videoCounterLable.text = @"00:00";
        videoCounterLable.hidden = false;
        sendVideoData = nil;
        videoSize = 0;
        
        [video_startButton setBackgroundImage:[UIImage imageNamed:@"video_start.png"] forState:UIControlStateNormal];
        isvideo_start = YES;
    }
}

@end