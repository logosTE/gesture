//
//  RecordingCellVC.h
//  GestureIOS
//
//  Created by Nirav vavadiya on 23/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordingCellVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *gestureImageView;
@property (weak, nonatomic) IBOutlet UIImageView *noOfSeeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *noOfFavImageView;

@property (weak, nonatomic) IBOutlet UILabel *noOfSeeLable;
@property (weak, nonatomic) IBOutlet UILabel *noOfFavLable;
@property (weak, nonatomic) IBOutlet UIImageView *certificateImageView;
@property (weak, nonatomic) IBOutlet UILabel *recordingTimeLable;


@end
