//
//  RecordingListVC.h
//  GestureIOS
//
//  Created by Nirav vavadiya on 23/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordingListVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
