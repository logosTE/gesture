//
//  SPLockOverlay.m
//  SuQian
//
//  Created by Suraj on 25/9/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "SPLockOverlay.h"

#define kLineColor [UIColor colorWithRed:97.0/255.0 green:191.0/255.0 blue:3.0/255.0 alpha:1.0]
#define kLineGridColor  [UIColor colorWithRed:97.0/255.0 green:191.0/255.0 blue:3.0/255.0 alpha:1.0]
#define kLineGridColor1  [UIColor colorWithRed:97.0/255.0 green:191.0/255.0 blue:3.0/255.0 alpha:0.50]

@implementation SPLockOverlay

@synthesize pointsToDraw;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
			self.backgroundColor = [UIColor clearColor];
			self.pointsToDraw = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    
    CGContextRef context2 = UIGraphicsGetCurrentContext();
	CGContextRef context1 = UIGraphicsGetCurrentContext();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 5.0);
    
	CGContextSetStrokeColorWithColor(context, kLineGridColor.CGColor);
    
    
    for(SPLine *line in self.pointsToDraw)
		{
			CGContextMoveToPoint(context, line.fromPoint.x, line.fromPoint.y);
            
			CGContextAddLineToPoint(context, line.toPoint.x, line.toPoint.y);
            
            CGContextStrokePath(context);
            
			CGFloat nodeRadius = 30;
            CGFloat nodeRadius1 = 32;
            CGFloat nodeRadius2 = 34;
			
			CGRect fromBubbleFrame = CGRectMake(line.fromPoint.x- nodeRadius/2, line.fromPoint.y - nodeRadius/2, nodeRadius, nodeRadius);
            CGRect fromBubbleFrame1 = CGRectMake(line.fromPoint.x- nodeRadius1/2, line.fromPoint.y - nodeRadius1/2, nodeRadius1, nodeRadius1);
            CGRect fromBubbleFrame2 = CGRectMake(line.fromPoint.x- nodeRadius2/2, line.fromPoint.y - nodeRadius2/2, nodeRadius2, nodeRadius2);
            
            
            CGContextSetFillColorWithColor(context2, kLineGridColor.CGColor);
           
            
            
            CGContextFillEllipseInRect(context2, fromBubbleFrame2);
           
            
            
			
			if(line.isFullLength){
                CGRect toBubbleFrame2 = CGRectMake(line.toPoint.x - nodeRadius2/2, line.toPoint.y - nodeRadius2/2, nodeRadius2, nodeRadius2);
                CGContextFillEllipseInRect(context2, toBubbleFrame2);
               
			}
            
            CGContextSetFillColorWithColor(context1, [UIColor whiteColor].CGColor);
            
            
            CGContextFillEllipseInRect(context1, fromBubbleFrame1);
            
            
            
            if(line.isFullLength){
                CGRect toBubbleFrame1 = CGRectMake(line.toPoint.x - nodeRadius1/2, line.toPoint.y - nodeRadius1/2, nodeRadius1, nodeRadius1);
                CGContextFillEllipseInRect(context1, toBubbleFrame1);
            }
            
            CGContextSetFillColorWithColor(context, kLineGridColor.CGColor);
            
            
            CGContextFillEllipseInRect(context, fromBubbleFrame);
            
            
            
            if(line.isFullLength){
                CGRect toBubbleFrame = CGRectMake(line.toPoint.x - nodeRadius/2, line.toPoint.y - nodeRadius/2, nodeRadius, nodeRadius);
                CGContextFillEllipseInRect(context, toBubbleFrame);
            }


		}
}
@end
