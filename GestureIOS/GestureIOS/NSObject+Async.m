//
//  NSObject+Async.m
//  GestureIOS
//
//  Created on 07/07/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "NSObject+Async.h"

@implementation NSObject (Async)

- (void) performSelectorAsync:(SEL) selector completion:(void(^)(id result))completion {
    if ([self respondsToSelector:selector]) {
    
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[[self class] instanceMethodSignatureForSelector:selector]];
            [invocation setSelector:selector];
            [invocation setTarget:self];
            
            __unsafe_unretained id unretainedResult = nil;
            [invocation invoke];
            [invocation getReturnValue:&unretainedResult];
            
            id result = unretainedResult;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Main Thread 
                completion(result);
            });
        });
    }
}

@end
