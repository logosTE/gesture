//
//  RecordingGestureVC.h
//  GestureIOS
//
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
// @@@ update by andre
@interface RecordingGestureVC : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
    AppDelegate *appDelegate;
    UIButton *btnExit;
    SLComposeViewController *mySLComposerSheet;
}

@property (weak, nonatomic) IBOutlet UIView *opaqueView;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (IBAction)onSubmit:(id)sender;
- (IBAction)onSubmitPost:(id)sender;
- (IBAction)onReplay:(id)sender;
- (IBAction)onCancel:(id)sender;

- (void) showAlertView:title;


@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
//@property (weak, nonatomic) IBOutlet UIButton *audioButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIImageView *bg_imageview;
@property (weak, nonatomic) IBOutlet UIImageView *Audio_MicImageview;
@property (weak, nonatomic) IBOutlet UILabel *audio_Lable;
@property (weak, nonatomic) IBOutlet UIButton *audio_button;
@property (weak, nonatomic) IBOutlet UIButton *flipCameraButton;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;

@property (weak, nonatomic) IBOutlet UIButton *gellaryButton;




- (IBAction)cameraButton_click:(id)sender;
- (IBAction)audioButton_click:(id)sender;
- (IBAction)videoButton_click:(id)sender;
- (IBAction)audio_start_Click:(id)sender;
- (IBAction)flipCamera_click:(id)sender;

- (IBAction)Exit_Click:(id)sender;

- (IBAction)gellaryButton_click:(id)sender;




@end
