//
//  LoginVC.h
//  GestureIOS
//
//  Created by Nirav vavadiya on 12/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "ProgressHUD.h"
#import "IQKeyboardReturnKeyHandler.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "GmailLikeLoadingView.h"
@class GPPSignInButton;
@interface LoginVC : UIViewController<GPPSignInDelegate>{
    AppDelegate *appDelegate;
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    GmailLikeLoadingView *redBlueView;
    UIView *ProgressView;
}
@property (weak, nonatomic) IBOutlet UIButton *SignInButton1;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtpwd;
@property (weak, nonatomic) IBOutlet UIButton *forgotpasswordButton;

@property (weak, nonatomic) IBOutlet UIImageView *emailchkImg;
@property (weak, nonatomic) IBOutlet UIImageView *pwdchkImg;
@property (weak, nonatomic) IBOutlet GPPSignInButton *signInButton;


- (IBAction)signUpbutton_click:(id)sender;
- (IBAction)signInButton_click:(id)sender;
- (IBAction)fbLogin:(id)sender;
- (IBAction)forgotPwd:(id)sender;

- (IBAction)twitterLogin:(id)sender;
- (IBAction)googleLogin:(id)sender;

- (void) showRedBlueProgressView;
- (void) hideRedBlueProgressView;

@end
