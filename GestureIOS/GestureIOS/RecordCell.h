//
//  RecordCell.h
//  GestureIOS
//

//

#import <UIKit/UIKit.h>

@interface RecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *recordTimeLable;
@property (strong, nonatomic) IBOutlet RecordCell *recordCell;

@property (weak, nonatomic) IBOutlet UIImageView *gestureImageView;
@property (weak, nonatomic) IBOutlet UIImageView *eyeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *favImageView;
@property (weak, nonatomic) IBOutlet UIImageView *certificateImageView;

@property (weak, nonatomic) IBOutlet UILabel *noOfEyeLable;
@property (weak, nonatomic) IBOutlet UILabel *noOfFavLable;

@property (weak, nonatomic) IBOutlet UILabel *noOfDaylable;


@end
