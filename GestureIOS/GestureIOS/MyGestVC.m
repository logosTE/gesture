//
//  MyGestVC.m
//  GestureIOS
//

//
@import GoogleMobileAds;
#import "MyGestVC.h"
#import "RecordingGestureVC.h"
#import "PlayGestureVC.h"
#import "NormalCircle.h"
#import "SPLockScreen.h"
#import "RecordingListTV.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "AppDelegate.h"
#import "NSObject+Async.h"

#define kCurrentPattern												@"KeyForCurrentPatternToUnlock"
#define kCurrentPatternTemp										@"KeyForCurrentPatternToUnlockTemp"


@interface MyGestVC ()<LockScreenDelegate>{
    NSString *patternchk;
    BOOL alredypatternchk;
    NSString *conformPattern;
    NSString *alredyconformpattern;
    NSArray *recordList;
//    NSArray *paths;
//    NSString *filename,*newPath;
    NSData * gesturedata;
    NSString *reversedString;
    NSString *reversedString2;
    NSMutableArray *arrowArray;
}
@property (nonatomic) NSInteger wrongGuessCount;

@end

@implementation MyGestVC
@synthesize infoLabelStatus,wrongGuessCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // @@@ add google ads by andre
    arrowArray = [[NSMutableArray alloc] init];
    self.bannerView.adUnitID = @"ca-app-pub-8972676458491669/5871034833";
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [self.bannerView loadRequest:request];
    
    appdelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.navigationItem.title = @"GestURe";
//    if (self.pattern == nil) {
//        self.navigationItem.hidesBackButton = YES;
//    }
    if ([self.isAddfromList isEqualToString:@"yes"]) {
        self.navigationItem.hidesBackButton = NO;
        UIBarButtonItem *mystatus = [[UIBarButtonItem alloc] initWithTitle:@"My Status" style:UIBarButtonItemStylePlain target:self action:@selector(mystatus_Click)];
        self.navigationItem.leftBarButtonItem = mystatus;
       
    }else{
        self.navigationItem.hidesBackButton = YES;
    }
    
//     currentUser = [PFUser currentUser];
    
    UIBarButtonItem *logoutBtn = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout_Click)];
    self.navigationItem.rightBarButtonItem = logoutBtn;
    self.navigationController.navigationBarHidden = NO;
    
    patternchk = @"";
    alredypatternchk = NO;
    alredyconformpattern = nil;
    appdelegate.pattern = nil;
    appdelegate.PatternImage = nil;
    
    // @@@ add by andre
    NSLog(@"%f", [[UIScreen mainScreen] bounds].size.height);
    if ([[UIScreen mainScreen ] bounds].size.height < 568){
        //iPhone 4s
        CGFloat offset = 20;
        CGFloat offset1 = 30;
        CGRect frame1 = self.infoLabel.frame;
        frame1.origin.y -= offset;
        self.infoLabel.frame = frame1;
        
        CGRect frame2 = self.gestureView.frame;
        frame2.origin.y -= offset;
        self.gestureView.frame = frame2;
        
        CGRect frame3 = self.restartButton.frame;
        frame3.origin.y -= (offset + offset1);
        self.restartButton.frame = frame3;
        
        CGRect frame4 = self.restartSymbol.frame;
        frame4.origin.y -= (offset + offset1);
        self.restartSymbol.frame = frame4;
        
        CGRect frame5 = self.continueButton.frame;
        frame5.origin.y -= (offset + offset1);
        self.continueButton.frame = frame5;
        
        CGRect frame6 = self.continueSymbol.frame;
        frame6.origin.y -= (offset + offset1);
        self.continueSymbol.frame = frame6;
    }
    self.lockScreenView = [[SPLockScreen alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width-50, self.view.bounds.size.width-50)];
    self.lockScreenView.delegate = self;
    self.lockScreenView.backgroundColor = [UIColor clearColor];
    [self.gestureView addSubview:self.lockScreenView];
    
    self.infoLabelStatus = InfoStatusFirstTimeSetting;
    
    [self updateOutlook];
}

-(void)mystatus_Click{
    [ProgressHUD show:@"Loading..."];
    RecordingListTV *rectList = [[RecordingListTV alloc]initWithNibName:@"RecordingListTV" bundle:nil];
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rectList] animated:YES completion:nil];
}

//@@@ add 09/04/15 by andre
- (void) viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [self restart];
}

- (void)updateOutlook
{
    switch (self.infoLabelStatus) {
        case InfoStatusFirstTimeSetting:
            self.infoLabel.text = @"Draw a pattern to share!";
            break;
        case InfoStatusConfirmSetting:
//            self.infoLabel.text = @"Confirm the Pattern !";
            break;
        case InfoStatusFailedConfirm:
//            self.infoLabel.text = @"Failed to confirm, please retry";
            break;
        case InfoStatusNormal:
//            self.infoLabel.text = @"Draw previously set pattern to go in";
            break;
        case InfoStatusFailedMatch:
//            self.infoLabel.text = [NSString stringWithFormat:@"Wrong Guess # %lu, try again !",(long)self.wrongGuessCount];
            break;
        case InfoStatusSuccessMatch:
//            self.infoLabel.text = @"Success match pattern!";
            break;
            
        default:
            break;
    }
    
}


#pragma -LockScreenDelegate

- (void)lockScreen:(SPLockScreen *)lockScreen didEndWithPattern:(NSString *)patternNumber
{
    
    if ([patternNumber isEqualToString:@""]) {
        self.infoLabelStatus = InfoStatusFirstTimeSetting;
        [self updateOutlook];
    }else{
        NSUserDefaults *stdDefault = [NSUserDefaults standardUserDefaults];
        switch (self.infoLabelStatus) {
            case InfoStatusFirstTimeSetting:
                [stdDefault setValue:patternNumber forKey:kCurrentPatternTemp];
                patternchk = @"1";
                 NSLog(@"1");
                alredyconformpattern = patternNumber;
                self.infoLabelStatus = InfoStatusConfirmSetting;
                break;
            case InfoStatusFailedConfirm:
                [stdDefault setValue:patternNumber forKey:kCurrentPatternTemp];
                self.infoLabelStatus = InfoStatusConfirmSetting;
                patternchk = @"1";
                [self updateOutlook];
                
                break;
            case InfoStatusConfirmSetting:
                if([patternNumber isEqualToString:[stdDefault valueForKey:kCurrentPatternTemp]]) {
                    [stdDefault setValue:patternNumber forKey:kCurrentPattern];

                    patternchk = @"2";
                    NSLog(@"2");
                    conformPattern = patternNumber;
                    self.infoLabelStatus = InfoStatusSuccessMatch;
                    [self updateOutlook];
                    
                    
                }
                else {
                    self.infoLabelStatus = InfoStatusFailedConfirm;
                    patternchk = @"3";
                    NSLog(@"3");
                    [self updateOutlook];
                }
                break;
            case  InfoStatusNormal:
                if([patternNumber isEqualToString:[stdDefault valueForKey:kCurrentPattern]]) [self dismissViewControllerAnimated:YES completion:nil];
                else {
                    self.infoLabelStatus = InfoStatusFailedMatch;
                    self.wrongGuessCount ++;
                    [self updateOutlook];
                }
                break;
            case InfoStatusFailedMatch:
                if([patternNumber isEqualToString:[stdDefault valueForKey:kCurrentPattern]]) [self dismissViewControllerAnimated:YES completion:nil];
                else {
                    self.wrongGuessCount ++;
                    self.infoLabelStatus = InfoStatusFailedMatch;
                    [self updateOutlook];
                }
                break;
            case InfoStatusSuccessMatch:
                [self dismissViewControllerAnimated:YES completion:nil];
                break;
                
            default:
                break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) logout_Click {

    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Are you sure?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Logout", nil];
    alert.tag = 100;
    [alert show];
    
}

-(void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [ProgressHUD show:@"Loading..."];
    if (buttonIndex == [alertView cancelButtonIndex])
    {
        if (alertView.tag == 100)
        {
            [ProgressHUD dismiss];
        }
    }
    else
    {
        if (alertView.tag == 100)
        {
            // @@@ add logout function newly 09/04/15 by andre
            [PFUser logOut];
            [[AppDelegate delegate] logout];
        }
        
    }
}

- (IBAction)restsrtButton_click:(id)sender
{
    [self restart];
}
- (void) restart
{
    patternchk = @"";
    alredyconformpattern = @"";
    alredypatternchk = NO;
    [self.lockScreenView resetScreen];
    self.infoLabelStatus = InfoStatusFirstTimeSetting;
    [self updateOutlook];
}
- (IBAction)continuebutton_click:(id)sender {
   
    if ([AppDelegate numberOfGestures] >= MAX_GESTURES){
        NSString *error = [NSString stringWithFormat:@"You reached the maximum number of uploads for your user type. You can only upload %@ gestures.", @(MAX_GESTURES)];
        [[[UIAlertView alloc] initWithTitle:@"Error" message:error  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    [ProgressHUD show:@"Checking..."];
   
    /**************** new code ************************/
    
    
    if (alredyconformpattern.length <= 0)
    {
        
        UIAlertView *alert=[[UIAlertView alloc ] initWithTitle:@"Required" message:@"Please draw pattern first." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        
        [alert show];
        [ProgressHUD dismiss];
        return;
    }
    else
    {
        
        [arrowArray removeAllObjects];
        NSArray *items = [alredyconformpattern componentsSeparatedByString:@","];
        NSUInteger countOfNewPattern = [items count];
        for (int i = 0; i < countOfNewPattern-1; i++)
        {
            NSString *item1 = [items objectAtIndex:i+1];
            NSString *item2 = [items objectAtIndex:i];
            NSUInteger num1 = [item1 integerValue];
            NSUInteger num2 = [item2 integerValue];
            int col1 = (int)((num1 - 1)/6);
            int col2 = (int)((num2 - 1)/6);
            NSString *directString;
            if (col1 == col2) {
                directString = @"L";
            }
            else
                directString = @"R";
            NSString *newString;
            if (num1 < num2)
            {
                newString = [NSString stringWithFormat:@"%@.%lu.%lu", directString, (unsigned long)num1, (unsigned long)num2];
            }
            else
                newString = [NSString stringWithFormat:@"%@.%lu.%lu", directString, (unsigned long)num2, (unsigned long)num1];
            [arrowArray addObject:newString];
        }
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:arrowArray];
        [arrowArray removeAllObjects];
        for(int i = 0; i<[tempArray count]; i++)
        {
            NSString *tempString = [tempArray objectAtIndex:i];
            if ([arrowArray count] == 0)
            {
                [arrowArray addObject:tempString];
            }
            else
            {
                BOOL isHas = NO;
                for (int j=0; j<[arrowArray count]; j++)
                {
                    NSString *arrowString = [arrowArray objectAtIndex:j];
                    if ([tempString isEqualToString:arrowString])
                    {
                        isHas = YES;
                    }
                    if (j == [arrowArray count]-1)
                    {
                        if (!isHas) {
                            [arrowArray addObject:tempString];
                        }
                    }
                    
                }

            }
        }
        NSUInteger newPatternCount = [arrowArray count];
        PFQuery *strquery = [PFQuery queryWithClassName:@"RecordingFiles"];
        [strquery whereKey:@"patterncount" equalTo:[NSNumber numberWithInteger:[arrowArray count]]];
        
        [strquery performSelectorAsync:@selector(findObjects) completion:^(NSArray * dbObjects) {
            
            //        BOOL isCheckedAll = YES;
            for (int i=0; i<[dbObjects count]; i++)
            {
                PFObject *object = [dbObjects objectAtIndex:i];
                NSString *arrowPatternString = [object objectForKey:@"patternstring"];
                NSArray *arrowPatternArray = [arrowPatternString componentsSeparatedByString:@","];
                if (newPatternCount == [arrowPatternArray count])
                {
                    BOOL isCheckedIn = YES;
                    for (int i = 0; i<[arrowArray count]; i++)
                    {
                        BOOL isCheckedIn2 = NO;
                        NSString *newPattern = [arrowArray objectAtIndex:i];
                        for (int j = 0; j<[arrowPatternArray count]; j++)
                        {
                            NSString *oldPattern = [arrowPatternArray objectAtIndex:j] ;
                            if ([newPattern isEqualToString:oldPattern])
                            {
                                isCheckedIn2 = YES;
                            }
                            if (j == [arrowPatternArray count]-1)
                            {
                                if (!isCheckedIn2) {
                                    isCheckedIn = NO;
                                }
                            }
                        }
                    }
                    if (isCheckedIn)
                    {
                        PlayGestureVC *play = [[PlayGestureVC alloc] initWithNibName:@"PlayGestureVC" bundle:nil];
                        NSString *filetype = [NSString stringWithFormat:@"%@",[object objectForKey:@"filetype"]];
                        play.playerType = filetype;
                        play.playerID = object.objectId;
                        
                        if ([filetype isEqualToString:@"Capture Image"])
                        {
                            PFFile *gestImageFile = [object objectForKey:@"uploadFile"];
                            PFFile *gestureImgFile = [object objectForKey:@"patternImg"];
                            [gestImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error)
                             {
                                 if (!error)
                                 {
                                     NSData *imageData = [gestImageFile getData];
                                     play.playFile = imageData;
                                     
                                     [gestureImgFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                                         play.gestureimageData = data;
                                         [self.navigationController pushViewController:play animated:YES];
                                         [ProgressHUD dismiss];
                                     }];
                                     
                                 }
                             }];
                            
                            
                        }
                        else if([filetype isEqualToString:@"video"])
                        {
                            PFFile *videoFile = [object objectForKey:@"uploadFile"];
                            [videoFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
                                if(!error){
                                    result = [videoFile getData];
                                    
                                    play.playFile = result;
                                    PFFile *gestureImgFile = [object objectForKey:@"patternImg"];
                                    [gestureImgFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                                        play.gestureimageData = data;
                                        [ProgressHUD dismiss];
                                        [self.navigationController pushViewController:play animated:YES];
                                    }];
                                    
                                }
                                
                            }];
                        }
                        else
                        {
                            PFFile *audioFile = [object objectForKey:@"uploadFile"];
                            [audioFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
                                if(!error){
                                    result = [audioFile getData];
                                    play.playFile = result;
                                    PFFile *gestureImgFile = [object objectForKey:@"patternImg"];
                                    [gestureImgFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                                        play.gestureimageData = data;
                                        [ProgressHUD dismiss];
                                        [self.navigationController pushViewController:play animated:YES];
                                    }];
                                }
                                
                            }];
                        }
                        return;
                    }
                    
                    
                }
                
                
            }
            NSString *uploadString = [arrowArray componentsJoinedByString:@","];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:uploadString forKey:@"patternstring"];
            [defaults synchronize];
            [self takescreenshotes];
            appdelegate.pattern = alredyconformpattern;
            self.infoLabelStatus = InfoStatusFirstTimeSetting;
            [self updateOutlook];
            RecordingGestureVC *record = [[RecordingGestureVC alloc] initWithNibName:@"RecordingGestureVC" bundle:nil];
            [self.navigationController pushViewController:record animated:YES];
            [ProgressHUD dismiss];
            NSLog(@"alreadyconformpattern%@",alredyconformpattern);
        }];
    }
        //[self.lockScreenView resetScreen];
}

-(void)takescreenshotes{
    
    UIGraphicsBeginImageContextWithOptions(self.gestureView.bounds.size, self.gestureView.opaque, 1.0);// capacity adjust
    
    [self.gestureView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGFloat original_Height = image.size.height;
    CGFloat original_Width = image.size.width;
    NSLog(@"Original Image Size : %f,%f",original_Width,original_Height);
    CGFloat rate = 1.0;
    // @@@ add change size of image that post to FB by andre
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(original_Width * rate,original_Height * rate);

    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    gesturedata = UIImagePNGRepresentation(tempImage);
    NSLog(@"After Image Size : %f,%f",tempImage.size.width,tempImage.size.height);
    appdelegate.PatternImage = gesturedata;
    [ProgressHUD dismiss];

}
@end
