//
//  FAQVC.m
//  GestureIOS
//
//  Created by admin111 on 4/10/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "FAQVC.h"
#import <MessageUI/MessageUI.h>
#import "ProgressHUD.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface FAQVC () <UIWebViewDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@end

@implementation FAQVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"FAQ";
    
    NSURL *termsURL = [[NSBundle mainBundle] URLForResource:@"GestURe - FAQ" withExtension:@"rtf"];
    [_webView loadRequest:[NSURLRequest requestWithURL:termsURL]];
    _webView.delegate = self;
    _webView.scalesPageToFit = true;
    
    self.contactButton.backgroundColor = self.navigationController.navigationBar.barTintColor;
}

#if TARGET_IPHONE_SIMULATOR

- (void)loadView
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    
    NSURL *termsURL = [[NSBundle mainBundle] URLForResource:@"GestURe - FAQ" withExtension:@"rtf"];
    [webView loadRequest:[NSURLRequest requestWithURL:termsURL]];
    webView.delegate = self;
    webView.scalesPageToFit = true;
    
    self.view = webView;
}

#endif

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    float rw = [UIScreen mainScreen].bounds.size.width / webView.scrollView.contentSize.width;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        rw *= 1.6;
    }
    
    webView.scrollView.zoomScale = rw;
}

- (IBAction) contactAction {
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"Notification to support team"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"support@logos-te.com", nil];
        [mailer setToRecipients:toRecipients];
        
        // Determine the MIME type
        NSString *mimeType;
        mimeType = @"image/png";
        
        //get the filepath from resources
        //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"patternimage" ofType:@"png"];
        
        NSString *emailBody = @"Issue : \n Solution proposal : \n Remark: ";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        mailer.view.tintColor = [UIColor whiteColor];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not open the mail composer"
                                                        message:@"Your device is not configured for sending e-mails. Please check e-mail configuration in Settings and try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [ProgressHUD showSuccess:@"Mail sent!"];
            break;
        case MFMailComposeResultFailed:
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"An unknown error has occured. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
