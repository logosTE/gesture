//
//  AppDelegate.h
//  GestureIOS
//
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
@class GTMOAuth2Authentication;

#define  MAX_GESTURES 5

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;


@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *loginType;
@property (nonatomic,strong) NSString *objId;
@property (nonatomic,strong) NSString *pattern;
@property (nonatomic,strong) NSData *PatternImage;

// @@@ add logout function newly 09/04/15 by andre
- (void) logout;
+(AppDelegate *) delegate;// @@@ add 09/04/15 by andre

+ (NSInteger) numberOfGestures;
+ (void) refreshNumberOfGestures;
@end

