//
//  PlayGestureVC.h
//  GestureIOS
//
 
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
@interface PlayGestureVC : UIViewController{
    AppDelegate *appDelegate;
    // @@@ add by andre
    SLComposeViewController *mySLComposerSheet;
}

@property (nonatomic) NSString *playerType;
@property (nonatomic) NSData *playFile;
@property (nonatomic) NSString *playerID;
@property (nonatomic) NSURL *playURL;
@property (nonatomic) NSData *gestureimageData;

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (strong, nonatomic)  UIImageView *imageCaptureView;

@end
