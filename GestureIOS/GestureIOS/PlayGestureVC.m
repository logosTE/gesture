//
//  PlayGestureVC.m
//  GestureIOS
//
 
//

#import "PlayGestureVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import "ProgressHUD.h"
#import "AppDelegate.h"
#import "NSObject+Async.h"
#import "ProgressHUD.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface PlayGestureVC ()<AVAudioRecorderDelegate, AVAudioPlayerDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>{
    UIButton *btnExit;
    UIButton *PlayButton;
    UIButton *likeButton;
    UIButton *reportButton;
    UIButton *postButton;// @@@ add by andre
    UIImageView *miceimg;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    BOOL islike;
    PFUser *currentUser;
    NSString *favID;
    MPMoviePlayerController *playerview;
    NSArray *paths;
    NSString *documentsDirectory ;
    NSString *path;
    NSURL *moveUrl;
    
    BOOL alreadyLoaded;
   
}

@end

@implementation PlayGestureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Player";
    
    self.imageCaptureView = [[UIImageView alloc] init];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    self.imageCaptureView.frame = CGRectMake(0, 0, width, height);
    [self.view addSubview:self.imageCaptureView];
    
//    self.navigationItem.hidesBackButton = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    /**************************************************************/
    //Exit Button
    btnExit  = [UIButton buttonWithType:UIButtonTypeSystem];
    btnExit.frame = CGRectMake(275 , 23, 32, 32);
    [btnExit setBackgroundImage:[UIImage imageNamed:@"exit@2x.png"] forState:UIControlStateNormal];
    btnExit.hidden = YES;
    [btnExit addTarget:self action:@selector(onExit) forControlEvents:UIControlEventTouchUpInside];
    //[self.playerView addSubview:btnExit];
    //PlayButton
    PlayButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    if ([[UIScreen mainScreen]bounds].size.height == 480){
        PlayButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -200 , [[UIScreen mainScreen]bounds].size.height - 280, 83, 77);
    }else{
        PlayButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -200 , [[UIScreen mainScreen]bounds].size.height - 350, 83, 77);
    }
    [PlayButton setBackgroundImage:[UIImage imageNamed:@"playButton.png"] forState:UIControlStateNormal];
    PlayButton.hidden = YES;
    [PlayButton addTarget:self action:@selector(btnplay) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:PlayButton];
//    [self.playerView addSubview:PlayButton];
    /***********************************************************/
    miceimg = [[UIImageView alloc] init];
//   [miceimg setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width -185 , [[UIScreen mainScreen]bounds].size.height - 500, 57, 87)];
    // @@@ add 09/04/15 by andre
    NSLog(@"%f,%f",[[UIScreen mainScreen]bounds].size.height,self.view.frame.size.width);
    if ([[UIScreen mainScreen]bounds].size.height == 480){
        [miceimg setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width -185 , PlayButton.frame.origin.y - 100, 57, 87)];
    }else{
        [miceimg setFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width -185 , PlayButton.frame.origin.y - 120, 57, 87)];
    }
    miceimg.image = [UIImage imageNamed:@"playMice.png"];
    miceimg.hidden = YES;
    [self.view addSubview:miceimg];
//    [self.playerView addSubview:miceimg];
    
    
    /***********************************************************/
    //likeButton
    likeButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    likeButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -80 , [[UIScreen mainScreen]bounds].size.height - 160, 60, 60);
    [likeButton setBackgroundImage:[UIImage imageNamed:@"dislike.png"] forState:UIControlStateNormal];
    
    [likeButton addTarget:self action:@selector(btnlike_dislike) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:likeButton];
//    [self.playerView addSubview:likeButton];
    /***********************************************************/
    /***********************************************************/
    //reportButton
    reportButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    reportButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -80 , [[UIScreen mainScreen]bounds].size.height - 235, 60, 60);
    [reportButton setBackgroundImage:[UIImage imageNamed:@"report_button.png"] forState:UIControlStateNormal];
    [reportButton addTarget:self action:@selector(btnreport) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reportButton];
//    [self.playerView addSubview:reportButton];
    /***********************************************************/
    //Post Button // @@@ add by andre
    postButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    postButton.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width -80 , [[UIScreen mainScreen]bounds].size.height - 310, 60, 60);
    [postButton setBackgroundImage:[UIImage imageNamed:@"fb@2x.png"] forState:UIControlStateNormal];
    [postButton addTarget:self action:@selector(onPost) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:postButton];
//    [self.playerView addSubview:postButton];
    /***********************************************************/
    self.imageCaptureView.hidden = YES;
    

    // Do any additional setup after loading the view from its nib.
}

// @@@ add by andre
- (void) onPost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:@"GestURe"]; //the message you want to post
        BOOL isUrl = [mySLComposerSheet addURL:[NSURL URLWithString:@"URL of this app"]];
        NSLog(@"isUrl=%@",[NSNumber numberWithBool:isUrl]);
        [mySLComposerSheet addImage:[UIImage imageWithData:self.gestureimageData]];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please login to Facebook." message:@"You have to login to Facebook first." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everything worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (alreadyLoaded){
        return;
    }
    
    alreadyLoaded = YES;
    
    self.imageCaptureView.hidden = YES;
    paths = nil;
    documentsDirectory = nil;
    path= nil;
    moveUrl= nil;
    currentUser = [PFUser currentUser];
 /**************************** No of See *****************************************/

        [ProgressHUD show:@"Loading..."];
        PFObject *testObject = [PFObject objectWithClassName:@"noOfSee"];
        testObject[@"recordFileID"] = self.playerID;
        testObject[@"userObjID"] = currentUser.objectId;
        //[testObject saveInBackground];
        
        [testObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error)
            {
                PFQuery *query = [PFQuery queryWithClassName:@"RecordingFiles"];
                [query whereKey:@"objectId" equalTo:self.playerID];
                [query getFirstObjectInBackgroundWithBlock:^(PFObject * userStats, NSError *error) {
                    if (!error) {
                        // Found UserStats
                        [userStats incrementKey:@"numberOfsee" byAmount:[NSNumber numberWithInt:1]];
                        
                        [ProgressHUD dismiss];
                        // send push
                        
                        // Save
                        [userStats saveInBackground];
                    } else {
                        // Did not find any UserStats for the current user
                        NSLog(@"Error: %@", error);
                        [ProgressHUD dismiss];
                        
                    }
                }];
                [ProgressHUD dismiss];
                
            }
        }];
        
//    }

    [self check_favWithCompletion:^{
        
        /********************************************************************/
        
        if([self.playerType isEqualToString:@"Capture Image"]){
            self.imageCaptureView.hidden = NO;
            btnExit.hidden = NO;
            UIImage *image = [UIImage imageWithData:self.playFile];
            
            [self.imageCaptureView setImage: image];
            self.imageCaptureView.contentMode = UIViewContentModeScaleToFill;
            
            if (image.imageOrientation == UIImageOrientationUp) {
                NSLog(@"portrait");
                self.imageCaptureView.contentMode = UIViewContentModeScaleAspectFit;
            } else if (image.imageOrientation == UIImageOrientationLeft || image.imageOrientation == UIImageOrientationRight) {
                NSLog(@"landscape");
                self.imageCaptureView.contentMode = UIViewContentModeScaleToFill;
            }
            
            
            miceimg.hidden = YES;
            PlayButton.hidden = YES;
            
            
        }else if([self.playerType isEqualToString:@"video"]){
            miceimg.hidden = YES;
            PlayButton.hidden = NO;
            
            /// display video
            paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
            path = [documentsDirectory stringByAppendingPathComponent:@"myMove.mov"];
            [[NSFileManager defaultManager] createFileAtPath:path contents:self.playFile attributes:nil];
            moveUrl = [NSURL fileURLWithPath:path];
            self.imageCaptureView.hidden = NO;
            
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:moveUrl options:nil];
            AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            generate.appliesPreferredTrackTransform=TRUE;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 60);
            CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
            //        NSLog(@"err==%@, imageRef==%@", err, imgRef);
            
            UIImage *thumbnail =  [[UIImage alloc] initWithCGImage:imgRef];
            
            self.imageCaptureView.image=thumbnail;
            self.imageCaptureView.contentMode = UIViewContentModeScaleAspectFit;
            playerview = [[MPMoviePlayerController alloc]init];
            [playerview setContentURL:moveUrl];
            playerview.controlStyle = MPMovieControlStyleNone;
            //        playerview.view.frame = self.playerView.bounds;
            //        [self.playerView addSubview:playerview.view];
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            [playerview.view setFrame:CGRectMake(0, 0, width, height)];
            [self.view addSubview:playerview.view];
            [playerview play];
            [playerview setFullscreen:YES animated:YES];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(onStopVideo)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:playerview];
            
            
        }else if ([self.playerType isEqualToString:@"Audio"]){
            //        self.playerView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"playAudioBg1.png"]];
            //        self.playerView.contentMode = UIViewContentModeScaleAspectFit;
            self.imageCaptureView.image = [UIImage imageNamed:@"playAudioBg1.png"];
            miceimg.hidden = NO;
            PlayButton.hidden = NO;
        }
    }];
}

-(void)check_favWithCompletion:(void(^)(void)) completion{
    /********************************************************************/
    /**************************** No of Fav *****************************/
    PFQuery *favquery = [PFQuery queryWithClassName:@"Favorites"];
    [favquery whereKey:@"userObjID" equalTo:currentUser.objectId];
    [favquery whereKey:@"recordFileID" equalTo:self.playerID];
    
    [favquery performSelectorAsync:@selector(findObjects) completion:^(NSArray* favArray) {
        
        if (favArray.count == 0) {
            
            [likeButton setBackgroundImage:[UIImage imageNamed:@"dislike.png"] forState:UIControlStateNormal];
            islike = NO;
        }else{
            [likeButton setBackgroundImage:[UIImage imageNamed:@"like.png"] forState:UIControlStateNormal];
            PFObject *obj=[favArray objectAtIndex:0];
            favID = obj.objectId;
            NSLog(@"%@",favID);
            
            islike = YES;
        }
    }];
    
    if (completion) {
        completion();
    }
}
- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.height);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

// @@@ add 09/04/15 by andre

- (void) onExit{
    [self.navigationController popViewControllerAnimated:YES];
}
NSString *dataString;
-(void) btnplay {
    if ([self.playerType isEqualToString:@"Audio"]) {
        player = [[AVAudioPlayer alloc] initWithData:self.playFile error:nil];
        player.volume = 16;
        [player setDelegate:self];
        
        [player play];
        PlayButton.hidden = YES;
    }else{
        
        playerview = [[MPMoviePlayerController alloc]init];
        [playerview setContentURL:moveUrl];
        playerview.controlStyle = MPMovieControlStyleNone;
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        [playerview.view setFrame:CGRectMake(0, 0, width, height)];
//        playerview.view.frame = self.playerView.bounds;
       
//        [self.playerView addSubview:playerview.view];
        [self.view addSubview:playerview.view];
        [playerview play];
        //[playerview setFullscreen:YES animated:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onStopVideo)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:playerview];
        
    }
}

-(void)onStopVideo
{
    [playerview.view removeFromSuperview];
}
-(void) viewWillDisappear:(BOOL)animated{
       [playerview stop];
}
- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    PlayButton.hidden = NO;
}
-(void) btnlike_dislike{
    
    [ProgressHUD show:@"Loading..."];
    if(islike == NO){
        [likeButton setBackgroundImage:[UIImage imageNamed:@"like.png"] forState:UIControlStateNormal];
        
        PFObject *testObject = [PFObject objectWithClassName:@"Favorites"];
        testObject[@"recordFileID"] = self.playerID;
        testObject[@"userObjID"] = currentUser.objectId;
        //[testObject saveInBackground];
        
        [testObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error)
            {
                PFQuery *query = [PFQuery queryWithClassName:@"RecordingFiles"];
                [query whereKey:@"objectId" equalTo:self.playerID];
                [query getFirstObjectInBackgroundWithBlock:^(PFObject * userStats, NSError *error) {
                    if (!error) {
                        // Found UserStats
                        [userStats incrementKey:@"numberOfFav" byAmount:[NSNumber numberWithInt:1]];
                        
                        
                        // send push
                        
                        // Save
                        [userStats saveInBackground];
                        [self check_favWithCompletion:^{                                                 [ProgressHUD dismiss];
                            islike = YES;
                        }];
                    } else {
                        // Did not find any UserStats for the current user
                        NSLog(@"Error: %@", error);
                        [ProgressHUD dismiss];
                        
                    }
                }];
                [ProgressHUD dismiss];
                
            }
        }];
        islike = YES;
    }else{
        [likeButton setBackgroundImage:[UIImage imageNamed:@"dislike.png"] forState:UIControlStateNormal];
        NSLog(@"dislike %@",favID);
         PFObject *delobject = [PFObject objectWithoutDataWithClassName:@"Favorites" objectId:favID];
         [delobject deleteEventually];
        
        PFQuery *query = [PFQuery queryWithClassName:@"RecordingFiles"];
        [query whereKey:@"objectId" equalTo:self.playerID];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject * userStats, NSError *error) {
            if (!error) {
             
                if([[userStats objectForKey:@"numberOfFav"]integerValue] != 0){
                    // Found UserStats
                    [userStats incrementKey:@"numberOfFav" byAmount:[NSNumber numberWithInt:-1]];
                    
                    
                    // send push
                    
                    // Save
                    [userStats saveInBackground];
                }
                [ProgressHUD dismiss];
            } else {
                // Did not find any UserStats for the current user
                NSLog(@"Error: %@", error);
                [ProgressHUD dismiss];
                
            }
        }];
        islike = NO;
    }
}
-(void)btnreport{
//    // Email Subject
//    NSString *emailTitle = @"GestureIOS";
//    // Email Content
//    NSString *messageBody = @"hello";
//    // To address
//    NSArray *toRecipents = [NSArray arrayWithObject:@"support@gmail.com"];
//
//    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//    mc.mailComposeDelegate = self;
//    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
//    [mc setToRecipients:toRecipents];
//    
//    // Present mail view controller on screen
//    
//    [self presentViewController:mc animated:YES completion:NULL];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"Notification to support team"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"support@logos-te.com", nil];
        [mailer setToRecipients:toRecipients];
        
        // Determine the MIME type
        NSString *mimeType;
        mimeType = @"image/png";
        
        //get the filepath from resources
        //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"patternimage" ofType:@"png"];
        
        //add attachement
        [mailer addAttachmentData:self.gestureimageData mimeType:mimeType fileName:@"patternimage.png"];
        
        NSString *emailBody = @"Issue : \n Solution proposal : \n Remark: ";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        mailer.view.tintColor = [UIColor whiteColor];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not open the mail composer"
                                                            message:@"Your device is not configured for sending e-mails. Please check e-mail configuration in Settings and try again."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [ProgressHUD showSuccess:@"Mail sent!"];
            break;
        case MFMailComposeResultFailed:
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"An unknown error has occured. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
