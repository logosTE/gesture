//
//  SignUpVC.h
//  GestureIOS
//

//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ProgressHUD.h"
@interface SignUpVC : UIViewController
{
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtUserID;
@property (weak, nonatomic) IBOutlet UITextField *txtemail;
@property (weak, nonatomic) IBOutlet UITextField *txtpwd;

@property (weak, nonatomic) IBOutlet UIImageView *nameChkImg;
@property (weak, nonatomic) IBOutlet UIImageView *userIdChkImg;
@property (weak, nonatomic) IBOutlet UIImageView *emailChkimg;
@property (weak, nonatomic) IBOutlet UIImageView *pwdChkImg;
- (IBAction)signUpButtonClick:(id)sender;
- (IBAction)signInButtonClick:(id)sender;
- (IBAction)termsBurronClick:(id)sender;
- (IBAction)onDisplayFAQ:(id)sender;

@end
