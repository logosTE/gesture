//
//  RecordingListVC.m
//  GestureIOS
//
//  Created by Nirav vavadiya on 23/10/1936 SAKA.
//  Copyright (c) 1936 SAKA xyz. All rights reserved.
//

#import "RecordingListVC.h"
#import "RecordingCellVC.h"
@interface RecordingListVC (){
    NSMutableArray *recordList;
    NSArray *record;
    RecordingCellVC *gestCell;
}

@end

@implementation RecordingListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"My Status";
    self.navigationItem.hidesBackButton = YES;
    record = [NSArray arrayWithObjects:@"00:00", @"00:02", nil];
    recordList = [[NSMutableArray alloc]init];
    [recordList addObjectsFromArray:record];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return record.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Dequeue the cell.
    static NSString *CellIdentifier  = @"Cell";
    RecordingCellVC *cell = (RecordingCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"RecordingCellVC" owner:self options:nil];
        cell = gestCell;
        
    }
//    RecordingCellVC *cell = [tableView dequeueReusableCellWithIdentifier:@"gestureCell"];;
//    if (cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecordingCellVC" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    // Set the loaded data to the appropriate cell labels.
    //cell.recordingTimeLable.text = [record objectAtIndex:indexPath.row];
    return cell;
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
